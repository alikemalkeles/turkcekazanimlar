import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter/services.dart';
import 'dart:convert';

import '../database/gitlab.dart';
import '../database/hiveDatabase.dart';
import '../kazanimlar/kazanimlariListele.dart';
import '../model/kazanimKaydetModel.dart';




class KazanimKaydet extends StatefulWidget {
  const KazanimKaydet({Key? key}) : super(key: key);


  @override
  // ignore: library_private_types_in_public_api
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<KazanimKaydet> {

  List<KazanimKmodeli> dersPlanlari = [];
  List<KazanimKmodeli> kursPlanlari = [];
  List<KazanimKmodeli> sesliMetinler = [];
  List<KazanimKmodeli> pdfKitaplar = [];

  var hiveDb = HiveDatabase();
  var gitlabDb = GitlabData();

  final String dosyaYolu =
      "https://gitlab.com/alikemalkeles/turkcekazanimlar/-/raw/main/kaynaklar/bulutDosyaYollari.json";

  Future<void> _launchInBrowser(Uri url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Adres bulunamadı $url';
    }
  }

  Future<void> textShare(String yol) async {
    await FlutterShare.share(
        title: 'Dosya indirme bağlantısını paylaş',
        text: "${yol}",
        chooserTitle: 'Dosya indirme bağlantısını paylaş');
  }

  Future bulutDosyaYolu() async {

    dynamic dataYollar = {};

    var resultHive = await hiveDb.getDataHive("yollar");
    // print("Hive den dönen sonuç: ${resultHive}");
    // EĞER Hive den null dönerse
    // 1. İnternete bağlan verileri al ve Hiveye yaz
    // 2. İnternet yok ise assets üzerinden al ve hiveye yaz
    if (resultHive == null) {
      var dataGitlab = await gitlabDb.getGitlabData(dosyaYolu);
      // İnternetten gelen data null ise hive ye ekle null değil ise assetsten datayı al
      if (dataGitlab == null) {
        print("Yollar Hive boş, internet bağlantısı kurulamadı, içerikler assetsden alınacak");
        // İnternetten de veri gelmediği için aseets den verileri al
        final assetsPlanlar = await rootBundle.loadString("assets/kaynaklar/bulutDosyaYollari.json");
        dataYollar = jsonDecode(assetsPlanlar) ?? {};
        await hiveDb.addDataHive("yollar", dataYollar);
      } else {
        print("Yollar Hive boş, içerikler internetten alınacak ve hiveye yazılacak");
        // İntrenetten data başarılı bir şekilde geldği için gelen data Hive'ye yazılacak.
        dataYollar  = await gitlabDb.getGitlabData(dosyaYolu);
        await hiveDb.addDataHive("yollar", dataYollar);
      }
    } else {
      // Hive de içerik bulunduğundan hive döndürelecek
      dataYollar = resultHive;
      print("Yollar Hive de içerik bulunuyor jsonType ${dataYollar.runtimeType}");
    }



    for (List ogeD in dataYollar["yollar"]["ders"]){
      if (ogeD.length == 4) {
          dersPlanlari.add(KazanimKmodeli(ogeD[0], ogeD[1], ogeD[2], ogeD[3]));
      } else{
        print("oge eksik ${ogeD}");
      }
    }
    for (List ogeK in dataYollar["yollar"]["kurs"]){
      if (ogeK.length == 4) {
        kursPlanlari.add(KazanimKmodeli(ogeK[0], ogeK[1], ogeK[2], ogeK[3]));
      } else{
        print("oge eksik ${ogeK}");
      }
    }
    for (List ogeS in dataYollar["yollar"]["sesliMetinler"]){
      if (ogeS.length == 4) {
        sesliMetinler.add(KazanimKmodeli(ogeS[0], ogeS[1], ogeS[2], ogeS[3]));
      } else{
        print("oge eksik ${ogeS}");
      }
    }
    for (List ogeP in dataYollar["yollar"]["kitapPdf"]){
      if (ogeP.length == 4) {
        pdfKitaplar.add(KazanimKmodeli(ogeP[0], ogeP[1], ogeP[2], ogeP[3]));

      } else{
        print("oge eksik ${ogeP}");
      }
    }
    print("${pdfKitaplar.length}");
    return "1";

  }


  @override
  void initState() {
    super.initState();
    bulutDosyaYolu().then((value) {
      setState(() {
        dersPlanlari;
      });
    });

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffDAE8FF),

      appBar: AppBar(
        elevation: 0.0,
        primary: true,
        toolbarHeight: 45,

        backgroundColor: const Color(0xffDAE8FF),
        centerTitle: false,
        title: const Text("Planlar ve Diğer Dokümanlar",
         style: TextStyle(color: Colors.black87, fontSize: 17, fontWeight: FontWeight.w400),
         ),
        shape:  const RoundedRectangleBorder(

           borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(0.0),
              bottomRight: Radius.circular(18.0)
           ),
         ),

        leading: IconButton(
            onPressed: (){
             Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>  KazanimlariListele(),
                    ),
                  );
            },
            icon: const Icon(Icons.arrow_back_ios, color: Colors.black)
        ),
      
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        margin: const EdgeInsets.fromLTRB(1.0, 1.0, 1.0, 0.0),
        padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 0.0),
        decoration: const BoxDecoration(
          color: Color(0xffDAE8FF),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18.0),
            topRight: Radius.circular(0.0)
          ),
        ),
        child: SingleChildScrollView(
          child: Theme(
            data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Ögeler
                 Container(
                   padding: const EdgeInsets.fromLTRB(3, 5, 3, 0),
                   margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                   decoration:  BoxDecoration(
                     color: Colors.white,
                     borderRadius: BorderRadius.circular(8.0),
                     gradient: const LinearGradient(
                         stops: [0.02, 0.02],
                         colors: [Colors.indigo, Colors.white]
                     ),
          
                    
                    boxShadow:[
                       BoxShadow(
                         color: Colors.black.withOpacity(0.1), //color of shadow
                         spreadRadius: 1, //spread radius
                         blurRadius: 3, // blur radius
                         offset: const Offset(0, 2), // changes position of shadow
                       ),
                       //you can set more BoxShadow() here
                     ], 
                   ),
                   child: ExpansionTile(
                     textColor: Colors.deepOrangeAccent,
                     expandedAlignment: Alignment.center,
                       collapsedIconColor: Colors.indigo,
                       iconColor: Colors.black,
                       leading: const Icon(Icons.my_library_books_outlined, size: 35, color: Colors.indigo,),
                       tilePadding: const EdgeInsets.all(7),
                     expandedCrossAxisAlignment: CrossAxisAlignment.center,
                     title: const Text("Ders Planları", style: TextStyle(color: Colors.indigo,  fontWeight: FontWeight.w600, fontSize: 18),),
                     subtitle: const Text("Ders planlarını cihazıma indir ya da indirme bağlantısını paylaş",
                       style: TextStyle(color: Colors.black,  fontWeight: FontWeight.w300, fontSize: 14),
                     ),
                     children: List.generate(dersPlanlari.length, (index) {
                       return ListTile(
                         contentPadding: const EdgeInsets.fromLTRB(7, 0, 5, 0),
                         dense: true,
                         title: Text("${dersPlanlari[index].sinif}.Sınıf ${dersPlanlari[index].yayinEvi}",
                           style: const TextStyle(fontSize: 15, fontWeight: FontWeight.w600, color: Colors.indigo),
                         ),
                           subtitle: Text("${dersPlanlari[index].bilgi}",
                             style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                           ),
                         trailing: Row(
                           mainAxisSize: MainAxisSize.min,
                           mainAxisAlignment: MainAxisAlignment.spaceAround,
                           children: [
                             IconButton(
                                 onPressed: (){
                                   _launchInBrowser(Uri.parse("${dersPlanlari[index].url}"));
                                 },
                                 icon: const Icon(Icons.cloud_download_outlined, color: Colors.indigo, size: 28,)
                             ),
                             IconButton(
                                 onPressed: (){
                                   textShare("${dersPlanlari[index].url}");
                                 },
                                 icon: const Icon(Icons.share, color: Colors.indigo, size: 28,)
                             )
                           ],
                         )
                
                       );
                      }
                     )
                   ),
                 ),
                Container(
                  padding: const EdgeInsets.fromLTRB(3, 0, 3, 0),
                  margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  decoration:  BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0),
                    gradient: const LinearGradient(
                        stops: [0.02, 0.02],
                        colors: [Colors.orange, Colors.white]
                    ),
                    boxShadow:[
                      BoxShadow(
                        color: Colors.black.withOpacity(0.1), //color of shadow
                        spreadRadius: 1, //spread radius
                        blurRadius: 3, // blur radius
                        offset: const Offset(0, 2), // changes position of shadow
                      ),
                      //you can set more BoxShadow() here
                    ],
                  ),
                  child: ExpansionTile(
                      textColor: Colors.deepOrangeAccent,
                      expandedAlignment: Alignment.center,
                      leading: const Icon(Icons.volume_up_outlined, size: 35, color: Colors.orange,),
                      iconColor: Colors.black,
                      collapsedIconColor: Colors.orange,
                      tilePadding: const EdgeInsets.all(7),
                      expandedCrossAxisAlignment: CrossAxisAlignment.center,
                      title: const Text("Dinleme/İzleme Metinleri", style: TextStyle(color: Colors.orange,  fontWeight: FontWeight.w600, fontSize: 18),),
                      subtitle: const Text("Dinleme/izleme metinlerini cihazıma indir ya da dosyanın indirme bağlantısını paylaş",
                        style: TextStyle(color: Colors.black54,  fontWeight: FontWeight.w300, fontSize: 14),
                      ),
                      children: List.generate(sesliMetinler.length, (index) {
                        return ListTile(
                            contentPadding: const EdgeInsets.fromLTRB(7, 0, 5, 0),
                            dense: true,
                            title: Text("${sesliMetinler[index].sinif}.Sınıf ${sesliMetinler[index].yayinEvi}",
                              style: const TextStyle(fontSize: 15, fontWeight: FontWeight.w600, color: Colors.orange),
                            ),
                            subtitle: Text("${sesliMetinler[index].bilgi}",
                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                            ),
                            trailing: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                IconButton(
                                    onPressed: (){
                                      _launchInBrowser(Uri.parse("${sesliMetinler[index].url}"));
                                    },
                                    icon: const Icon(Icons.cloud_download_outlined, color: Colors.orange, size: 28,)
                                ),
                                IconButton(
                                    onPressed: (){
                                      textShare("${sesliMetinler[index].url}");
                                    },
                                    icon: const Icon(Icons.share, color: Colors.orange, size: 28,)
                                )
                              ],
                            )
                
                        );
                      }
                      )
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(3, 0, 3, 0),
                  margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  decoration:  BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0),
                    gradient: const LinearGradient(
                        stops: [0.02, 0.02],
                        colors: [Colors.pink, Colors.white]
                    ),
                    boxShadow:[
                      BoxShadow(
                        color: Colors.black.withOpacity(0.1), //color of shadow
                        spreadRadius: 1, //spread radius
                        blurRadius: 3, // blur radius
                        offset: const Offset(0, 2), // changes position of shadow
                      ),
                      //you can set more BoxShadow() here
                    ],
                  ),
                  child: ExpansionTile(
                      textColor: Colors.deepOrangeAccent,
                      expandedAlignment: Alignment.center,
                      leading: const Icon(Icons.picture_as_pdf_outlined, size: 35, color: Colors.pink,),
                      iconColor: Colors.black,
                      collapsedIconColor: Colors.pink,
                      tilePadding: const EdgeInsets.all(7),
                      expandedCrossAxisAlignment: CrossAxisAlignment.center,
                      title: const Text("Ders Kitapları PDF", style: TextStyle(color: Colors.pink,  fontWeight: FontWeight.w600, fontSize: 18),),
                      subtitle: const Text("Ders kitaplarının PDF'sini cihazıma indir ya da indirme bağlantısını paylaş",
                        style: TextStyle(color: Colors.black54,  fontWeight: FontWeight.w300, fontSize: 14),
                      ),
                      children: List.generate(pdfKitaplar.length, (index) {
                        return ListTile(
                            contentPadding: const EdgeInsets.fromLTRB(7, 0, 5, 0),
                            dense: true,
                            title: Text("${pdfKitaplar[index].sinif}.Sınıf ${pdfKitaplar[index].yayinEvi}",
                              style: const TextStyle(fontSize: 15, fontWeight: FontWeight.w600, color: Colors.pink),
                            ),
                            subtitle: Text("${pdfKitaplar[index].bilgi}",
                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                            ),
                            trailing: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                IconButton(
                                    onPressed: (){
                                      _launchInBrowser(Uri.parse("${pdfKitaplar[index].url}"));
                                    },
                                    icon: const Icon(Icons.cloud_download_outlined, color: Colors.pink, size: 28,)
                                ),
                                IconButton(
                                    onPressed: (){
                                      textShare("${pdfKitaplar[index].url}");
                                    },
                                    icon: const Icon(Icons.share, color: Colors.pink, size: 28,)
                                )
                              ],
                            )
                
                        );
                      }
                      )
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(3, 0, 3, 0),
                  margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  decoration:  BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0),
                    gradient: const LinearGradient(
                        stops: [0.02, 0.02],
                        colors: [Colors.teal, Colors.white]
                    ),
                    boxShadow:[
                      BoxShadow(
                        color: Colors.black.withOpacity(0.1), //color of shadow
                        spreadRadius: 1, //spread radius
                        blurRadius: 3, // blur radius
                        offset: const Offset(0, 2), // changes position of shadow
                      ),
                      //you can set more BoxShadow() here
                    ],
                  ),
                  child: ExpansionTile(
                      textColor: Colors.deepOrangeAccent,
                      expandedAlignment: Alignment.center,
                      leading: const Icon(Icons.collections_bookmark_outlined, size: 35, color: Colors.teal,),
                      collapsedIconColor: Colors.teal,
                      iconColor: Colors.black,
                      tilePadding: const EdgeInsets.all(7),
                      expandedCrossAxisAlignment: CrossAxisAlignment.center,
                      title: const Text("Kurs Planları", style: TextStyle(color: Colors.teal,  fontWeight: FontWeight.w600, fontSize: 18),),
                      subtitle: const Text("Kurs planlarını cihazıma indir ya da indirme bağlantısını paylaş",
                        style: TextStyle(color: Colors.black,  fontWeight: FontWeight.w300, fontSize: 14),
                      ),
                      children: List.generate(kursPlanlari.length, (index) {
                        return ListTile(
                            contentPadding: const EdgeInsets.fromLTRB(7, 0, 5, 0),
                            dense: true,
                            title: Text("${kursPlanlari[index].sinif}.Sınıf ${kursPlanlari[index].yayinEvi}",
                              style: const TextStyle(fontSize: 15, fontWeight: FontWeight.w600, color: Colors.teal),
                            ),
                            subtitle: Text("${kursPlanlari[index].bilgi}",
                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                            ),
                            trailing: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                IconButton(
                                    onPressed: (){
                                      _launchInBrowser(Uri.parse("${kursPlanlari[index].url}"));
                                    },
                                    icon: const Icon(Icons.cloud_download_outlined, color: Colors.teal, size: 28,)
                                ),
                                IconButton(
                                    onPressed: (){
                                      textShare("${kursPlanlari[index].url}"
                                      );
                                    },
                                    icon: const Icon(Icons.share, color: Colors.teal, size: 28,)
                                )
                              ],
                            )
                
                        );
                      }
                      )
                  ),
                ),
                
                Container(
                  padding: const EdgeInsets.fromLTRB(3, 0, 3, 0),
                  margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  decoration:  BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(8.0),
                    border: Border.all(color: Colors.transparent)
                  ),
                  child: ListTile(
                      textColor: Colors.black87,
                      dense: true,
                      contentPadding: EdgeInsets.zero,
                      leading: const RotatedBox(quarterTurns: 2,child: Icon(Icons.wb_incandescent_outlined, size: 28, color: Colors.amber)),
                      iconColor: Colors.black,
                      subtitle: Text.rich(
                          TextSpan(
                              style: const TextStyle(fontSize: 14,),
                              children: [
                                const TextSpan(
                                    text: "Ders ve kurs planları "
                                ),
                                TextSpan(
                                    style: const TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                                    //make link blue and underline
                                    text: "turkcedersi.net",
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () async {
                                        String url = "https://www.turkcedersi.net";
                                        if (!await launchUrl(
                                          Uri.parse(url),
                                          mode: LaunchMode.externalApplication,
                                        )) {
                                          throw 'Adres bulunamadı $url';
                                        }
                                      }
                                ),
                                const TextSpan(
                                    text: " sitesindeki içeriklerden yararlanılarak oluşturulmuştur."
                                ),
                
                                //more text paragraph, sentences here.
                              ]
                          )
                      )
                
                  ),
                ),
                
                
              ],
            ),
          ),
        )
      ),
      
    );
  }





}


