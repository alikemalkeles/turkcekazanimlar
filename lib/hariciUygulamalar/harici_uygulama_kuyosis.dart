import 'package:auto_size_text/auto_size_text.dart';
import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter/services.dart';
import 'dart:convert';

import '../database/gitlab.dart';
import '../database/hiveDatabase.dart';
import '../kazanimlar/kazanimlariListele.dart';
import '../model/kazanimKaydetModel.dart';

class HariciUygulamalarKuyosis extends StatefulWidget {
  const HariciUygulamalarKuyosis({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _HariciUygulamalarKuyosis createState() => _HariciUygulamalarKuyosis();
}


class _HariciUygulamalarKuyosis extends State<HariciUygulamalarKuyosis> {

  
  
  String kuyosis_indirme_yolu = "https://drive.google.com/drive/folders/1WSS1wB3QoqcxiPhTY8qKSppowmMFMHjn?usp=sharing";

  Future<void> _launchInBrowser(Uri url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Adres bulunamadı $url';
    }
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/resimler/arkplan1.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          toolbarHeight: 35,
          centerTitle: true,
          elevation: 0.0,
          title: const Text(
            "Kütüphane Yönetim Sistemi",
            style: TextStyle(
              color: Colors.white,
              fontSize: 15,
              fontWeight: FontWeight.w600
            ),
          ),
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(Icons.arrow_back_ios, color: Colors.white)),
        ),
        body: bilgilendirme(context),
      ),
    );
  }



  Widget bilgilendirme(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // Icon(Icons.menu_book, color: Colors.pinkAccent, size: 100,),
          Padding(
            padding: const EdgeInsets.fromLTRB(90, 0, 90, 4),
            child: Image.asset("assets/resimler/kuyosis -2.png")
          ),

          // Uygulama Adı
          BlurryContainer(
            blur: 15,
            elevation: 0.0,
            color: Colors.white70.withOpacity(0.2),
            padding: const EdgeInsets.fromLTRB(40, 3, 40, 3),
            borderRadius: const BorderRadius.all(Radius.circular(30)),
            child: const Text(
              "KÜYÖSİS",
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.w800),
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(14, 25, 14, 15),
            child: BlurryContainer(
              //width: MediaQuery.of(context).size.width -,
              blur: 17,
              elevation: 0.0,
              color: Colors.blueAccent.withOpacity(0.3),
            
              padding: const EdgeInsets.fromLTRB(10,10, 10, 10),
              borderRadius:
              const BorderRadius.all(Radius.circular(10)),
              child:  Column(
                children: [
                 const Padding(
                   padding: EdgeInsets.all(8.0),
                   child: AutoSizeText(
                     "Uygulama ücretsizdir",
                     minFontSize: 12,
                     maxFontSize: 18,
                     textAlign: TextAlign.left,
                     style: TextStyle(
                         fontSize: 15,
                         color: Colors.red,
                         fontWeight: FontWeight.w600),
                   ),
                 ),

                  const AutoSizeText(" Bilgilendirme: İndirme işlemi tarayıcı üzerinden yapılacaktır. Uygulamayı indir butonuna tıkladıktan sonra indirme sayfasına yönlendirileceksiniz. Bu sayfadan güncel sürümü seçip uygulamayı cihazınıza indirebilirsiniz.",
                    minFontSize: 12,
                    maxFontSize: 18,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 12,
                        color: Colors.black,
                        fontWeight: FontWeight.w300),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: ElevatedButton.icon(
                      icon: const Icon(Icons.download_outlined, color: Colors.indigo),
                      label: const Text("Uygulamayı İndir",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.w600, color: Colors.indigo)),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.white.withOpacity(0.8)),
                      ),
                      onPressed: () {
                        _launchInBrowser(Uri.parse(kuyosis_indirme_yolu));
                      },
                    ),
                  ),


                ],
              ),
            ),
          ),

          // Hakkında
          Padding(
            padding: const EdgeInsets.fromLTRB(14, 25, 14, 20),
            child: BlurryContainer(
              blur: 16,
              elevation: 0.0,
              color: Colors.transparent,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              padding: const EdgeInsets.all(0.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white70.withOpacity(0.3),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 15, 8, 15),
                  child: Column(
                    children: [
                      const ListTile(
                          dense: true,
                          leading: Icon(Icons.developer_board_outlined,
                              color: Colors.pinkAccent, size: 28),
                          title: Text("Uygulama Geliştiricisi",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black)),
                          subtitle: Text(
                            "Ali Kemal KELEŞ",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black),
                          )),
                      const ListTile(
                          dense: true,
                          leading: Icon(Icons.new_releases_outlined,
                              color: Colors.pinkAccent, size: 28),
                          title:  Text("Güncel Sürüm",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black)),
                          subtitle: Text(
                            "1.2",
                            style:  TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black),
                          )),
                      ListTile(
                          dense: true,
                          leading: SizedBox(
                            width: 29,
                            child: IconButton(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                              icon: const Icon(
                                Icons.email_outlined,
                                size: 28,
                                color: Colors.pinkAccent,
                              ),
                              onPressed: () {

                              },
                            ),
                          ),
                          title: const Text("İletişim",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black)),
                          subtitle: const Text(
                            "alikemalkeles@yaani.com\nalikemalkeles@gmail.com",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black),
                          )),
                      ListTile(
                          dense: true,
                          leading: SizedBox(
                            width: 29,
                            child: IconButton(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                              icon: const Icon(
                                Icons.add_box_outlined,
                                size: 28,
                                color: Colors.pinkAccent,
                              ),
                              onPressed: () {

                              },
                            ),
                          ),
                          title: const Text("Gereksinimler",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black87)),
                          subtitle: const Text(
                            "Pardus 21 veya Pardus 23 sürüm\nPython 3.9 sonrası",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black87),
                          )
                      ),

                    ],
                  ),
                ),
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(14, 25, 14, 20),
            child: BlurryContainer(
              blur: 10,
              elevation: 0,
              color: Colors.transparent,
              borderRadius: const BorderRadius.all(Radius.circular(12)),
              padding: const EdgeInsets.all(0.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white60.withOpacity(0.3),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 15, 8, 15),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      BlurryContainer(
                        //width: MediaQuery.of(context).size.width -,
                        blur: 17,
                        elevation: 0.0,
                        color: Colors.blueAccent.withOpacity(0.3),

                        padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
                        borderRadius:
                        const BorderRadius.all(Radius.circular(30)),
                        child: const AutoSizeText(
                          "KÜYÖSİS HAKKINDA",
                          minFontSize: 12,
                          maxFontSize: 18,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.black,
                              fontWeight: FontWeight.w500),
                        ),
                      ),

                      const Padding(
                        padding: EdgeInsets.fromLTRB(8, 15, 8, 0),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            """  Bu uygulama kütüphanede kitap alıp verme işlemlerini kolaylaştırmak amacıyla gönüllülük esasına dayanarak oluşturulmuştur. Uygulama yalınzca Pardus’ta (Linux türevi diğer işletim sistemlerinde de) çalışır. Windows işletim sistemi desteği yoktur.\n  Uygulama halen daha geliştirme aşamasındadır. Bir problemle karşılaştığınızda hataları bildirmenizi istirham ederiz. Ayrıca uygulamada istediğiniz bir özelllik varsa talepte bulunabilirsiniz. Uygulamada oluşabilecek problemlere ve taleplerinize en kısa zamanda cevap vereceğimden hiç şüpheniz olmasın.\n Uygulama:\n Arayüzü: PyQt6\n Veritabanı: Sqlite
                    """,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),

                      BlurryContainer(
                        //width: MediaQuery.of(context).size.width -,
                        blur: 17,
                        elevation: 0.0,
                        color: Colors.blueAccent.withOpacity(0.3),

                        padding: const EdgeInsets.fromLTRB(30,5, 30, 5),
                        borderRadius:
                        const BorderRadius.all(Radius.circular(30)),
                        child: const AutoSizeText(
                          "ÖZELLİKLER",
                          minFontSize: 12,
                          maxFontSize: 18,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.black,
                              fontWeight: FontWeight.w600),
                        ),
                      ),

                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Wrap(
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children:  [
                            Icon(
                              Icons.arrow_right_outlined,
                              size: 40,
                              color: Colors.pink,
                            ),
                            Text(
                              "Kitap İşlemleri",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      ),

                      const Padding(
                        padding: EdgeInsets.fromLTRB(14, 0, 8, 15),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            """
● Kitap ekleme
● Kitap silme
● Kitap güncelleme
● Kitap karekod oluşturma
● Yayınevi, yazar, tür ve raf konumu filtreleme
● Emanetteki kitapları listeleme
● Arama ve filtreleme sonuçlarını excel dosyasına aktarma
● Çoklu kitap ekleme
● Çoklu olarak yayınevi, yazar ve raf konumu güncelleme (ayarlar sayfasından)
● Kitap emanet süresini belirleme (ayarlar sayfasından) """,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),

                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Wrap(
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children:  [
                            Icon(
                              Icons.arrow_right_outlined,
                              size: 40,
                              color: Colors.pink,
                            ),
                            Text(
                              "Üye İşlemleri",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      ),

                      const Padding(
                        padding: EdgeInsets.fromLTRB(14, 0, 8, 15),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            """
● Üye ekleme
● Üye silme
● Üye güncelleme
● Sınıf, şube, sınıf/şube filtreleme
● Üyenin okuduğu kitapları görünteleme ve excel dosyasına aktarma
● Çoklu üye ekleme
● Üyenin alabileceği kitap sayısını belirleme (ayarlar sayfasından)
● Otomatik sınıf yükseltme ve son sınıfı mezun etme""",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),

                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Wrap(
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children:  [
                            Icon(
                              Icons.arrow_right_outlined,
                              size: 40,
                              color: Colors.pink,
                            ),
                            Text(
                              "Kitap Alıp Verme İşlemleri",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      ),

                      const Padding(
                        padding: EdgeInsets.fromLTRB(14, 0, 8, 15),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            """
● Günlük verilen kitapları listeleme
● Günlük alınan kitapları listeleme
● Alıp verilen tüm kitapları tarihe göre listeleme
● Verilen kitaplar içinde detaylı arama yapma (üye adı, soyadı, öğrenci no, T.C. kimlik no, kitap adı, kitap karekod no)
● Verilen kitabı gün içinde iptal etme (Yalnızca yönetici iptal etme hakkına sahiptir.)
● Teslim süresi geçmiş kitapları listeleme
● Arama sonuçlarını excel dosyasına aktarma""",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),

                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Wrap(
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children:  [
                            Icon(
                              Icons.arrow_right_outlined,
                              size: 40,
                              color: Colors.pink,
                            ),
                            Text(
                              "Diğer İşlemler",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      ),

                      const Padding(
                        padding: EdgeInsets.fromLTRB(14, 0, 8, 15),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            """
● Kütüphane kullanıcı hesabı oluşturma ve yetki belirleme
● Veritabanını yedekleme ve yedeği geri yükleme
● Güncellemeleri uzaktan kontrol etme
● Excel dosyası üzerinden üye ekleme ve kitap ekleme
● İstatiksel işlemleri görüntüleme
   * Toplam kitap sayısı
   * Toplam üye sayısı
   * Alıp verilen toplam kitap sayısı
   * Cinsiyete göre okuma oranı
   * Sınıf seviyesine göre okuma oranı
   * Aylara göre okuma oranı
   * En çok okunan kitaplar
   * En çok okuyan üyeler""",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),

                    ],
                  ),
                ),
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(14, 25, 14, 20),
            child: BlurryContainer(
              blur: 16,
              elevation: 0.0,
              color: Colors.transparent,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              padding: const EdgeInsets.all(0.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white70.withOpacity(0.3),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 15, 8, 15),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                        child: BlurryContainer(
                          //width: MediaQuery.of(context).size.width -,
                          blur: 17,
                          elevation: 0.0,
                          color: Colors.blueAccent.withOpacity(0.9),
                          padding: const EdgeInsets.fromLTRB(30,5, 30, 5),
                          borderRadius:
                          const BorderRadius.all(Radius.circular(30)),
                          child: const AutoSizeText(
                            "Uygulama Ekran Görüntüleri",
                            minFontSize: 12,
                            maxFontSize: 18,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                      
                      // Ekran görüntüleri
                      ...List.generate(18, (index) {
                        return Container(
                          margin: const EdgeInsets.all(4),
                          child: Image.asset(
                            'assets/kuyosis_resimler/${index}.png',
                            fit: BoxFit.cover,
                          ),
                        );
                      })
                      

                    ],
                  ),
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }

}
