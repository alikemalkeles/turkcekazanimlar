import 'dart:async';
import 'dart:math';
//import 'dart:ui';
import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:numberpicker/numberpicker.dart';

// ignore: must_be_immutable
class YazimYanlislariOyun extends StatefulWidget {
  // ignore: prefer_final_fields
  List<dynamic> _tumKelimeler;
  YazimYanlislariOyun(@required this._tumKelimeler, {Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _YazimYanlislariOyun createState() => _YazimYanlislariOyun(_tumKelimeler);
}

class _YazimYanlislariOyun extends State<YazimYanlislariOyun> {
  // secilen kelime listesi
  List<dynamic> tumKelimeler = [];
  _YazimYanlislariOyun(this.tumKelimeler);
  // soru sırasını tutan degisken
  int soruSiraNo = 0;
  // soru siklari
  List<String> soruMaddeleri = ["---", "---"];

  String siradakiSorununCevabi = "";

  var secilenCevap = null;
  // Sonuc gösterme ikonlarının cevaba göre sekillenecegi degisken
  // 1 dogru, 0 yanlıs, 2 cevap bekleniyor
  var dogruYanlis = 2;
  // Cevap değitirilmesin diye toogleButton üzeri seffaf widgetle sarılır.
  // Bunu kontrol etmek icin kullanılacak . Acılısta False degerini alacak ve cevap verildikten sonra
  // True degerini alacak
  bool visibleWidgetDurum = false;

  // dogru ve yanlıs sayısı
  int dogruSayisi = 0;
  int yanlisSayisi = 0;

  //  sonraki soruya otomatik geciş butonu aktif durumu acılısta aktif
  bool otomatikGecis = true;

  // Yeni oyun baslat widget agacını aktif edecek olan visibility widget görünürlüğünü tutan değişken
  bool yeniOyunPenceresiBaslat = false;

  List<int> zamanlar = [10, 15, 20, 30, 40, 60, 90, 120, 180, 300];
  int zaman = 60;
  // Tooglebutton secim index
  int secilenSure = 60;
  int indexTogle = 5;
  late Timer _timer;

  void startTimer() {
    // print("zaman startTimer durumu: ${zaman}");
    const saniye = Duration(seconds: 1);
    _timer = Timer.periodic(
      saniye,
      (Timer timer) {
        if (zaman == 0) {
          // print("zaman if durumu: ${zaman}");
          setState(() {
            // Oyun bitince oyuna ait widgetleri gizle
            yeniOyunPenceresiBaslat = false;
            // süre bitince oyun sonuc penceresini çağır ve zamanı varsayılan toggle buton secimine göre yenideb ayarla
            zaman = secilenSure; // zamanlar[indexTogle];
            oyunSonucPenceresi(context);

            timer.cancel();
          });
        } else {
          setState(() {
            zaman--;
          });
        }
      },
    );
  }

  @override
  void initState() {
    // Gelistirici penceresi ögeleri
    tumKelimeler.shuffle();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      print("dispose ediliyor 3 ${_timer.isActive}");
      _timer.cancel();
    }
    super.dispose();
  }

  void soruGoster() {
    if (soruSiraNo >= 0 && soruSiraNo <= tumKelimeler.length) {
      List secilenSoru = tumKelimeler[soruSiraNo];
      // Sırayla soru seçimi yapılınca soru sıra numarasını bir arttır .
      // Eğer liste uzunluğundan büyük ise sayı soru sıra numarasını sıfırla
      if (soruSiraNo < tumKelimeler.length) {
        soruSiraNo++;
      } else {
        // Eğer soru sıra no 0 soru sayısına eşit veya büyükse soru sıra noyu sıfırla ve listeyi yeniden karıştır
        soruSiraNo = 0;
        tumKelimeler.shuffle();
        print(
            "Soru sayısının sonuna gelindiği için sıra başa alındı________________ ${soruSiraNo}");
      }

      // List secilenSoru = tumKelimeler[Random().nextInt(tumKelimeler.length)];
      print("Sıradaki sorunun maddeleri ${secilenSoru}");
      print("Sıradaki sorunun cevabı: ${secilenSoru[1]}");
      siradakiSorununCevabi = secilenSoru[1];

      // Yeni soru gelince secilen sık indexini sıfırla cünkü bu index durumuna göre
      // sonraki butonu aktif olacak
      secilenCevap = null;
      // Random modülüyle 0-1 arasında rakam sec ve soru maddesini rastgele oluştur
      int rastgeleMadde = Random().nextInt(2);

      setState(() {
        print("Rastgele soru maddesi değeri: ${rastgeleMadde}");
        // soru maddelerini karıştır ve madde değişkenine gönder
        soruMaddeleri = [
          secilenSoru[rastgeleMadde],
          secilenSoru[rastgeleMadde == 1 ? 0 : 1]
        ];

        print("Siradaki sorunun karısık maddeleri: ${soruMaddeleri}");
        // yeni soru acıldığında visibleWidgeti devre dısı bırak
        visibleWidgetDurum = false;

        dogruYanlis = 2;
        //soruSiraNo ++;
      });
    } else {
      print("Soruların sonuna geldik");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:const Color(0xffDAE8FF),
      appBar: AppBar(
        title: const Text('HANGİSİ DOĞRU?',
        style: TextStyle(color: Colors.black87, fontSize: 17, fontWeight: FontWeight.w500)
        ),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor:const Color(0xffDAE8FF),
        toolbarHeight: 42.0,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back_ios, color: Colors.black)),
      ),
      body: Container(
         margin:const  EdgeInsets.fromLTRB(1.0, 2.0, 1.0, 0.0),
          decoration: const BoxDecoration(
        color: Color(0xffDAE8FF),
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(0.0), topLeft: Radius.circular(18.0)),
      ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            //Süre secimi yazısı
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IgnorePointer(
                  ignoring: yeniOyunPenceresiBaslat,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.teal[400],
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(1, 2, 1, 2),
                      child: NumberPicker(
                        axis: Axis.horizontal,
                        value: secilenSure,
                        minValue: 10,
                        step: 10,
                        itemWidth: 45,
                        itemHeight: 40,
                        textStyle: const TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                        selectedTextStyle: const TextStyle(
                            color: Colors.white,
                            fontSize: 19,
                            fontWeight: FontWeight.w700),
                        itemCount: 5,
                        maxValue: 300,
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            border: Border.all(color: Colors.white70, width: 1),
                            borderRadius: BorderRadius.circular(12)),
                        onChanged: (value) => setState(() {
                          secilenSure = value;
                          zaman = value;
                        }),
                      ),
                    ),
                  ),
                ),
                // SÜre secimi ve zaman sayacı
                Padding(
                  padding: const EdgeInsets.fromLTRB(5, 4, 8, 12),
                  child: CircularStepProgressIndicator(
                    totalSteps: secilenSure, //zamanlar[indexTogle],
                    currentStep: zaman,
                    stepSize: 4,
                    selectedColor: Colors.teal[400],
                    unselectedColor: Colors.blueGrey[200],
                    padding: 0,
                    width: (MediaQuery.of(context).size.width / 8) * 2.2,
                    height: (MediaQuery.of(context).size.width / 8) * 2.2,
                    selectedStepSize: (MediaQuery.of(context).size.width / 30),
                    roundedCap: (_, __) => true,
                    child: Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: AutoSizeText(
                            "${zaman.toString()}",
                            maxFontSize: 50,
                            minFontSize: 18,
                            style: TextStyle(
                                color: Colors.teal[400],
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                          ),
                        )),
                  ),
                ),
              ],
            ),

            // Yeni oyun butonu ile bu widget agacı aktif edecek
            Visibility(
                visible: yeniOyunPenceresiBaslat,
                child: oyunPenceresi(context)),
            // soru cevaplanınca sonraki soruya otomatik geçiş aktif etme

            // Yeni Oyun Butonu
            Visibility(
              visible: yeniOyunPenceresiBaslat == true ? false : true,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        print("Soru sırası eski ${soruSiraNo}");
                        soruSiraNo = 0;
                        dogruSayisi = 0;
                        yanlisSayisi = 0;
                        yeniOyunPenceresiBaslat = true;
                        tumKelimeler.shuffle();
                        soruGoster();
                        startTimer();
                        print("Soru sırası yeni ${soruSiraNo}");
                      });
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.teal[400],
                      elevation: 5,
                      shadowColor: Colors.black38,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0),
                      ),
                    ),
                    child: const Row(
                      mainAxisSize: MainAxisSize.min,
                      children:  [
                        Icon(
                          Icons.play_arrow_rounded,
                          color: Colors.white,
                          size: 35,
                        ),
                        Text(" Başla  ",
                            style:
                                TextStyle(color: Colors.white, fontSize: 18)),
                      ],
                    )),
              ),
            ),

            Visibility(
              visible: yeniOyunPenceresiBaslat,
              child: // Anlık sonuc tabelası
                  Padding(
                padding: const EdgeInsets.fromLTRB(5, 2, 5, 1),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    BlurryContainer(
                      borderRadius: BorderRadius.circular(22),
                      color: Colors.white,
                      elevation: 0,
                      width: 260,
                      height: 38,
                      blur: 0,
                      padding: const EdgeInsets.fromLTRB(4, 4, 4, 4),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // Doğru paneli
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              const Text("Doğru",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500)),
                              CircleAvatar(
                                backgroundColor: Colors.greenAccent,
                                child: Text("${dogruSayisi.toString()}",
                                    maxLines: 1,
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ],
                          ),
                          const SizedBox(width: 70),
                          // Yanlış paneli
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              CircleAvatar(
                                backgroundColor: Colors.redAccent,
                                child: Text(" ${yanlisSayisi.toString()} ",
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold)),
                              ),
                              const Text("Yanlış",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500)),
                            ],
                          ),
                        ],
                      ),
                    ),
                    CircleAvatar(
                      backgroundColor:  Colors.white,

                      // ignore: sort_child_properties_last
                      child: Text(
                        "$soruSiraNo",
                        style: const TextStyle(
                            fontSize: 27,
                            fontWeight: FontWeight.w700,
                            color: Colors.black),
                      ),
                      minRadius: 25,
                      maxRadius: 25,
                    )
                  ],
                ),
              ),
            ),

            //const Spacer(),

            // soru cevaplanınca sonraki soruya otomatik geçiş aktif etme
            // Oyun başlayınca bu widget ağacındak ögeleri tıklnamaz yapma
            IgnorePointer(
              ignoring: yeniOyunPenceresiBaslat,
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(5, 0, 20, 3),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      const Text(
                        "Sonraki soruya otomatik geçiş   ",
                        style: TextStyle(color: Colors.black54, fontSize: 15),
                      ),
                      Switch(
                        //trackColor: MaterialStateProperty.all<Color>(Colors.greenAccent),
                        thumbColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                        inactiveThumbColor: Colors.white,
                        inactiveTrackColor: Colors.black26,
                        value: otomatikGecis,
                        onChanged: (value) {
                          setState(() {
                            otomatikGecis = value;
                            print(otomatikGecis);
                          });
                        },
                        activeTrackColor: Colors.teal[400],
                        activeColor: Colors.teal[400],
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // Yeni oyuna göre cağrılacak pencere
  Widget oyunPenceresi(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        // Hangisi dogru yazısı
        const  Padding(
          padding: EdgeInsets.fromLTRB(4, 5, 4, 5),
          child: Text(
            "Hangisi Doğru?",
            style: TextStyle(
                fontSize: 22, color: Colors.black, fontWeight: FontWeight.w500),
          ),
        ),
        // ana widget
        IgnorePointer(
          ignoring: visibleWidgetDurum,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 4, 8, 8),
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: ToggleSwitch(
                inactiveBgColor: Colors.teal[400],
                inactiveFgColor: Colors.white,
                activeFgColor: Colors.white,
                isVertical: true,
                minWidth: MediaQuery.of(context).size.width,
                minHeight: 68,
                fontSize: 22,

                radiusStyle: true,
                cornerRadius: 10.0,
                initialLabelIndex: secilenCevap,
                activeBgColors: const [
                  [Colors.orange],
                  [Colors.orange],
                ],
                labels: soruMaddeleri,
                animate: true,
                animationDuration: 150,
                changeOnTap: false,
                onToggle: (index) {
                  setState(() {
                    secilenCevap = index;
                    visibleWidgetDurum = true;
                    print("SEÇİLEN MADDE ${secilenCevap}");
                    if (siradakiSorununCevabi == soruMaddeleri[secilenCevap]) {
                      print("Cevap dogru");
                      dogruYanlis = 1;
                      dogruSayisi += 1;
                      // Otomatik gecis 0 ise yani evetse sonraki soruya 1 saniye sonra gec
                      if (otomatikGecis == true) {
                        Future.delayed(const Duration(milliseconds: 1100), () {
                          soruGoster();
                        });
                      }
                    } else {
                      print("Cevap Yanlıs");
                      dogruYanlis = 0;
                      yanlisSayisi += 1;
                      // Otomatik gecis 0 ise yani evetse sonraki soruya 1 saniye sonra gec
                      if (otomatikGecis == true) {
                        Future.delayed(const Duration(milliseconds: 1100), () {
                          soruGoster();
                        });
                      }
                    }
                  });
                  print('switched to: $index');
                },
              ),
            ),
          ),
        ),
        // cevap bekleniyor yazısı
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Wrap(
            alignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            spacing: 7.0,
            children: [
              if (dogruYanlis == 2) ...[
                const Icon(
                  Icons.timer_outlined,
                  color: Colors.black,
                  size: 32,
                ),
                const Text(
                  "Cevap bekleniyor...",
                  style: TextStyle(fontSize: 20, color: Colors.black),
                )
              ] else if (dogruYanlis == 1) ...[
                Icon(
                  Icons.done_outline_rounded,
                  color: Colors.green[400],
                  size: 32,
                ),
                const Text(
                  "Doğru",
                  style: TextStyle(
                      fontSize: 19,
                      color: Colors.green,
                      fontWeight: FontWeight.w500),
                )
              ] else if (dogruYanlis == 0) ...[
                Icon(
                  Icons.cancel_outlined,
                  color: Colors.redAccent[400],
                  size: 32,
                ),
                Text(
                  "Yanlış",
                  style: TextStyle(
                      fontSize: 19,
                      color: Colors.redAccent[400],
                      fontWeight: FontWeight.w400),
                )
              ]
            ],
          ),
        ),
        // Sonraki soru butonu
        Padding(
          padding: const EdgeInsets.all(6.0),
          child: Align(
            alignment: Alignment.center,
            // Otomatik geçiş aktifse eğer sonraki butonunu gizle
            child: Visibility(
              visible: otomatikGecis == false ? true : false,
              child: ElevatedButton(
                  onPressed: secilenCevap == null ? null : soruGoster,
                  style: ElevatedButton.styleFrom(
                    elevation: 4,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  child: const Row(
                    mainAxisSize: MainAxisSize.min,
                    children:  [
                      Text(
                        "Sonraki",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      ),
                      Icon(
                        Icons.skip_next,
                        color: Colors.white,
                        size: 32,
                      ),
                    ],
                  )),
            ),
          ),
        ),

        // Soru sırası ve dogru yanlıs sayısı
      ],
    );
  }

  Future oyunSonucPenceresi(BuildContext context) {
    print(
        "${MediaQuery.of(context).size.width}-${MediaQuery.of(context).size.height}");
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, StateSetter setState) {
          return SimpleDialog(
            backgroundColor: Colors.transparent,
            children: [
              BlurryContainer(
                height: MediaQuery.of(context).size.width / 1.5,
                width: MediaQuery.of(context).size.width,
                color: Colors.white.withOpacity(0.4),
                borderRadius: BorderRadius.circular(10),
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                blur: 10,
                elevation: 0.9,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    BlurryContainer(
                      padding: const EdgeInsets.fromLTRB(25, 2, 25, 2),
                      color: Colors.white.withOpacity(0.4),
                      elevation: 0,
                      blur: 15,
                      borderRadius: BorderRadius.circular(20),
                      child: const Text(
                        "OYUN SONUÇLARI",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 15, 0, 5),
                      child: Row(children: [
                        const AutoSizeText(
                          "   Soru sayısı:  ",
                          minFontSize: 15,
                          maxFontSize: 20,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: Colors.black),
                        ),
                        Text(
                          "${soruSiraNo}",
                          style: const TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                              color: Colors.black),
                        ),
                        const Spacer(),
                        const Icon(Icons.timer_outlined),
                        Text(
                          " ${zaman}",
                          style: const TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 22,
                              color: Colors.black),
                        ),
                        const Text(
                          "sn   ",
                          style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                              color: Colors.black),
                        ),
                      ]),
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          // Doğru Penceresi
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                              child: BlurryContainer(
                                color: Colors.white70.withOpacity(0.3),
                                blur: 15,
                                borderRadius: BorderRadius.circular(15),
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      const Text(
                                        "Doğru",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 20,
                                            color: Colors.white),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.fromLTRB(
                                            10, 5, 10, 0),
                                        color: Colors.white,
                                        height: 1,
                                      ),
                                      const Spacer(),
                                      AutoSizeText(
                                        "${dogruSayisi}",
                                        minFontSize: 40,
                                        maxFontSize: 120,
                                        wrapWords: true,
                                        maxLines: 1,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 50,
                                            color: Colors.greenAccent),
                                      ),
                                      const Spacer(),
                                    ]),
                              ),
                            ),
                          ),

                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                              child: BlurryContainer(
                                color: Colors.white70.withOpacity(0.3),
                                blur: 15,
                                borderRadius: BorderRadius.circular(15),
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      const Text(
                                        "Yanlış",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 20,
                                            color: Colors.white),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.fromLTRB(
                                            10, 5, 10, 0),
                                        color: Colors.white,
                                        height: 1,
                                      ),
                                      const Spacer(),
                                      AutoSizeText(
                                        "${yanlisSayisi}",
                                        minFontSize: 40,
                                        maxFontSize: 120,
                                        wrapWords: true,
                                        maxLines: 1,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 50,
                                            color: Colors.redAccent),
                                      ),
                                      const Spacer(),
                                    ]),
                              ),
                            ),
                          ),

                          //
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
        });
      },
    );
  }

  //
}
