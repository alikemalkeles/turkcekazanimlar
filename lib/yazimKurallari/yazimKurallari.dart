import 'dart:convert';
import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_scrolling_fab_animated/flutter_scrolling_fab_animated.dart';
import 'package:auto_size_text/auto_size_text.dart';

import '../database/gitlab.dart';
import '../database/hiveDatabase.dart';
import '../kazanimlar/kazanimlariListele.dart';
import 'yazimKurallariOyun.dart';



// ignore: use_key_in_widget_constructors
class YazimKurallari extends StatefulWidget {

  @override
  // ignore: library_private_types_in_public_api
  _YazimKurallari createState() => _YazimKurallari();
}

class _YazimKurallari extends State<YazimKurallari> {
  
  List tumKelimeler = [];

  var hiveDb = HiveDatabase();
  var gitlabDb = GitlabData();                

  final String dosyaYolu =
      "https://gitlab.com/alikemalkeles/turkcekazanimlar/-/raw/main/kaynaklar/yazimiYanlislar.json";
  // Genisşleyebilen float action button kaydırma durumunu kontrol eden kontrolcü
  // ignore: prefer_final_fields
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    jsonRead().then((value) {
      setState(() {
      //  print("JsonData dan gelen ${value}");
        tumKelimeler = value;
        print("Genislik: ${tumKelimeler.length}");
      });
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  // Yazımı yanlıs olan kelime listesi
  Future jsonRead() async {
    
    var resultHive = await hiveDb.getDataHive("kelimeler");

   // print(resultHive);
    // print("Hive den dönen sonuç: ${resultHive}");
    // EĞER Hive den null dönerse
    // 1. İnternete bağlan verileri al ve Hiveye yaz
    // 2. İnternet yok ise assets üzerinden al ve hiveye yaz
    if (resultHive == null) {
      var dataGitlab = await gitlabDb.getGitlabData(dosyaYolu);
      // İnternetten gelen data null ise hive ye ekle null değil ise assetsten datayı al
      if (dataGitlab == null) {
        print("Yollar Hive boş, internet bağlantısı kurulamadı, içerikler assetsden alınacak");
        // İnternetten de veri gelmediği için aseets den verileri al
        final assetsPlanlar = await rootBundle.loadString("assets/kaynaklar/yazimiYanlislar.json");
        var dataJson = jsonDecode(assetsPlanlar) ?? [];
        //tumKelimeler = dataJson["kelimeler"];
        await hiveDb.addDataHive("kelimeler", dataJson);
        return dataJson["kelimeler"];
      } else {
        print("Yollar Hive boş, içerikler internetten alınacak ve hiveye yazılacak");
        // İntrenetten data başarılı bir şekilde geldği için gelen data Hive'ye yazılacak.
        var dataJson2  = await gitlabDb.getGitlabData(dosyaYolu);
        tumKelimeler = dataJson2["kelimeler"] ?? [];
        await hiveDb.addDataHive("kelimeler", dataJson2);
        return dataJson2["kelimeler"] ?? [];
       
      }
    } else {
      // Hive de içerik bulunduğundan hive döndürelecek
      print("Yollar Hive de içerik bulunuyor jsonType ${tumKelimeler.runtimeType}");
      return resultHive["kelimeler"];
      
    }


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffDAE8FF),
      appBar: AppBar(
        elevation: 0.0,
        toolbarHeight: 40,
        backgroundColor: const Color(0xffDAE8FF),
        titleSpacing: 0.0,
        title: const AutoSizeText(
          "Yazımı Karıştırılan Kelimeler",
          minFontSize: 14,
          maxFontSize: 18,
          style: TextStyle(color: Colors.black87, fontSize: 16, fontWeight: FontWeight.w400),
        ),
        leading: IconButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const KazanimlariListele(),
                  ));
            },
            icon: const Icon(Icons.arrow_back_ios, color: Colors.black87)),

      actions: [
        Padding(
          padding: const EdgeInsets.fromLTRB(1, 4, 6, 4),
          child: Container(
            height: 25,
            padding: const EdgeInsets.fromLTRB(8, 2, 8, 2),
            decoration: BoxDecoration(
              color: Colors.transparent,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.black87, width: 1)
            ),
            child: Center(
              child: Text(
                "${tumKelimeler.length ?? 0} Öge",
                style: const TextStyle(color: Colors.black87),
              ),
            ),
            ),
        )
      ],
      ),

      body: Container(
        margin:const  EdgeInsets.fromLTRB(1.0, 2.0, 1.0, 0.0),
        decoration: const BoxDecoration(
          color: Color(0xffDAE8FF),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(0.0), topLeft: Radius.circular(18.0)),
        ),
        child: ListView.builder(
            padding: const EdgeInsets.fromLTRB(0, 15, 0, 5),
            controller: _scrollController,
            itemCount: tumKelimeler.length,
            itemBuilder: (BuildContext context, int index) {
              return yazimYanlislari(context, tumKelimeler[index]);
            }),
      ),
      floatingActionButton: ScrollingFabAnimated(
        height: 48,
        width: 155, 
        color: const Color(0xffDAE8FF),
        icon: const Icon(Icons.games_outlined, color: Colors.black87, size: 18),
        text: const Text(
          'Kendini Sına ',
          style: TextStyle(
              color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w400),
        ),
        onPress: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => YazimYanlislariOyun(tumKelimeler),
            ),
          );
        },
        scrollController: _scrollController,
        animateIcon: true,
        inverted: false,
        radius: 27.0,
        elevation: 10,
      ),
    );
  }

  Widget yazimYanlislari(BuildContext context, List<dynamic> oge) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(4, 3, 4, 3),
      // ignore: avoid_unnecessary_containers, sized_box_for_whitespace
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            Icons.close,
            color: Colors.red[600],
            size: 20,
          ),
          // ignore: sized_box_for_whitespace
          Container(
            width: MediaQuery.of(context).size.width / 2 - 28,
            child: AutoSizeText(" ${oge[0]} ",
              minFontSize: 11,
              maxLines: 1,
                maxFontSize: 16,
              style: const TextStyle(color: Colors.red, fontSize: 16),
            ),
          ),

          const Spacer(),

          // ignore: sized_box_for_whitespace
          Container(
            width: MediaQuery.of(context).size.width / 2 - 28,
            child: AutoSizeText("${oge[1]} ",
                textAlign: TextAlign.right,
                minFontSize: 11,
                maxLines: 1,
                maxFontSize: 16,
                style:
                    const TextStyle(color: Colors.green, fontSize: 16)),
          ),
          const Icon(
            Icons.done,
            color: Colors.green,
            size: 22,
          )
        ],
      ),
    );
  }
}
