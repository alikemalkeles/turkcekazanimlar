import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:blurrycontainer/blurrycontainer.dart';

import '../kazanimlar/kazanimlariListele.dart';


class DestekteBulunun extends StatelessWidget {
  const DestekteBulunun({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/resimler/arkplan1.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          elevation: 0.0,
          toolbarHeight: 0,
          backgroundColor: Colors.transparent,
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.fromLTRB(1.0, 2.0, 1.0, 0.0),
          decoration: const BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(12.0),
                topLeft: Radius.circular(12.0)),
          ),
          child: bagisHakkinda(context),
        ),
      ),
    );
    
  }

  Widget bagisHakkinda(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: IconButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => KazanimlariListele(),
                      ));
                },
                icon: const Icon(Icons.arrow_back_ios, color: Colors.white)),
          ),
          const Padding(
            padding: const EdgeInsets.all(0.0),
            child: Icon(Icons.volunteer_activism,
                color: Colors.pinkAccent, size: 80),
          ),
          const Padding(
            padding: EdgeInsets.all(3.0),
            child: Text(
              "Bize Destek Olun!",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w800,
                  fontSize: 22),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12),
            child: BlurryContainer(
              blur: 6,
              padding: const EdgeInsets.all(0),
              borderRadius: BorderRadius.circular(12),
              child: Container(
                padding: const EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  color: Colors.white54.withOpacity(0.1),
                  borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    topLeft: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0),
                  ),
                ),
                child: const Text(
                  """   \n Sayın Zümrelerim,\n Sizlere sürekli olarak hizmet sunabilmemiz için desteğinize ihtiyacımız var.\n\n""",
                  softWrap: true,
                  style: TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.w300,
                      fontSize: 17),
                ),
              ),
            ),
          ),
          Container(
              margin: EdgeInsets.fromLTRB(12, 8, 12, 8),
              padding: EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                color: Colors.blue.withOpacity(0.0),
                borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(10.0),
                  topLeft: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "     Ziraat Bankası",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Colors.pinkAccent),
                      )),
                  ElevatedButton.icon(
                    icon: const Icon(Icons.copy),
                    label: const Text("TR730001 0009 6854 4692 3750 01",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w600)),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Colors.blue.withOpacity(0.2)),
                    ),
                    onPressed: () {
                      Clipboard.setData(const ClipboardData(
                          text: "TR730001 0009 6854 4692 3750 01"));
                      _dialog(context);
                    },
                  ),
                  const SizedBox(
                    height: 7,
                  ),
                  const Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "     Akbank",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Colors.pinkAccent),
                      )),
                  ElevatedButton.icon(
                    icon: const Icon(Icons.copy),
                    label: const Text("TR69 004 6001 9688 8000 1705 85",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w600)),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Colors.blue.withOpacity(0.2)),
                    ),
                    onPressed: () {
                      Clipboard.setData(const ClipboardData(
                          text: "TR69 004 6001 9688 8000 1705 85"));
                      _dialog(context);
                    },
                  ),
                  const Align(
                    alignment: Alignment.centerLeft,
                    child: Text("     Ali Kemal KELEŞ",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.italic)),
                  )
                ],
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: const [
              Chip(
                  elevation: 4.0,
                  backgroundColor: Colors.pinkAccent,
                  avatar: Icon(
                    Icons.coffee_outlined,
                    color: Colors.white,
                    size: 17,
                  ),
                  label: Text(
                    "Çay 5₺",
                    style: TextStyle(color: Colors.white),
                  )),
              Chip(
                  elevation: 4.0,
                  backgroundColor: Colors.pinkAccent,
                  avatar: Icon(
                    Icons.coffee,
                    color: Colors.white,
                    size: 17,
                  ),
                  label: Text(
                    "Kahve 10₺",
                    style: TextStyle(color: Colors.white),
                  )),
              Chip(
                  elevation: 4.0,
                  backgroundColor: Colors.pinkAccent,
                  avatar: Icon(
                    Icons.restaurant,
                    color: Colors.white,
                    size: 17,
                  ),
                  label: Text(
                    "Yemek 20₺",
                    style: TextStyle(color: Colors.white),
                  ))
            ],
          )
        ],
      ),
    );
  }

  //
   void _dialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        Future.delayed(Duration(milliseconds: 600), () {
          // do something here
          Navigator.of(context).pop();
          // do stuff
        });
        return AlertDialog(
            backgroundColor: Colors.transparent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(40),
            ),
            content: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Icon(
                  Icons.done_all_sharp,
                  size: 45,
                  color: Colors.lightGreen,
                ),
                Text(
                  "Kopyalandı",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.lightGreen,
                      fontSize: 25,
                      fontWeight: FontWeight.w800),
                ),
              ],
            ));
      },
    );
  }


}




 

  
  

