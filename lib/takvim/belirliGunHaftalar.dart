import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:auto_size_text/auto_size_text.dart';

import '../database/gitlab.dart';
import '../database/hiveDatabase.dart';
import '../kazanimlar/kazanimlariListele.dart';
import '../model/takvimModel.dart';
import 'belirliGunHaftalarListe.dart';


class TakvimApp extends StatelessWidget {
  const TakvimApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage() : super();

  final String title = "Belirli Gün ve Haftalar";

  @override
  // ignore: library_private_types_in_public_api
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<Takvim> buAyinPlanlari = [];
  List<Takvim> senelikTakvimOgeleri = [];

  var hiveDb = HiveDatabase();
  var gitlabDb = GitlabData();

  final String dosyaYolu =
      "https://gitlab.com/alikemalkeles/turkcekazanimlar/-/raw/main/kaynaklar/akademikTakvim.json";

  /*
    - Yesil: Dini bayramlar            kontrol: 1
    - Mavi : belirli gün ve haftalar   kontrol: 2
    - Turuncu: Resmi bayramlar         kontrol: 3
    - kırmızı: okul ile ilgili         kontrol: 4
   */

  final simdi = DateTime.now();

  // Yazımı yanlıs olan kelime listesi
  Future<List<Takvim>> senelikTakvimOgeleriniAlFonk() async {

    dynamic dataTakvim = {};

     var resultHive = await hiveDb.getDataHive("takvim");
    // print("Hive den dönen sonuç: ${resultHive}");
    // EĞER Hive den null dönerse
    // 1. İnternete bağlan verileri al ve Hiveye yaz
    // 2. İnternet yok ise assets üzerinden al ve hiveye yaz
    if (resultHive == null) {
      var dataGitlab = await gitlabDb.getGitlabData(dosyaYolu);
      // İnternetten gelen data null ise hive ye ekle null değil ise assetsten datayı al
      if (dataGitlab == null) {
        print("Takvim Hive boş, internet bağlantısı kurulamadı, içerikler assetsden alınacak");
        // İnternetten de veri gelmediği için aseets den verileri al
        final assetsTakvim = await rootBundle.loadString("assets/kaynaklar/akademikTakvim.json");
        dataTakvim = jsonDecode(assetsTakvim) ?? {};
        await hiveDb.addDataHive("takvim", dataTakvim);
      } else {
        print("Takvim Hive boş, içerikler internetten alınacak ve hiveye yazılacak");
        // İntrenetten data başarılı bir şekilde geldği için gelen data Hive'ye yazılacak.
        dataTakvim = await gitlabDb.getGitlabData(dosyaYolu);
        await hiveDb.addDataHive("takvim", dataTakvim);
      }
    } else {
      // Hive de içerik bulunduğundan hive döndürelecek
      dataTakvim = resultHive;
      print("Takvim Hive de içerik bulunuyor jsonType ${dataTakvim.runtimeType}");
    }


   
    List tumTakvimOgeleri = dataTakvim["takvim"];
    // print("Tum Kelimeler: ${tumTakvimOgeleri.runtimeType} \n ${tumTakvimOgeleri}");
    for (List o in tumTakvimOgeleri) {
      if (o.length < 10 && o[0] == int && o[1] == int && o[7] == List) {
        print("Eksik__: ${o}");
      } else {
        //print("${oge[0].runtimeType}, ${oge[1].runtimeType}, ${oge[2].runtimeType}, ${oge[3].runtimeType}, ${oge[4].runtimeType}, ${oge[5].runtimeType}, ${oge[6].runtimeType}, ${oge[7].runtimeType}, ${oge[8].runtimeType}, ${oge[9].runtimeType}");
        senelikTakvimOgeleri.add(
            Takvim(o[0], o[1], o[2], o[3], o[4], o[5], o[6], o[7], o[8], o[9]));
        print("takvim");

      }
    }
    // Yıllık takvim ogelerini aldıktan sonra aylık takvim ogelerini cağır
    List<Takvim> buAykiler = [];
    buAykiler.clear();
    // Yıl içindeki hangi haftada olduğumuzu belirle
    final date =  DateTime.now();
    // ay numarasının bulunduğu önemli günleri listeye gönder
    senelikTakvimOgeleri.forEach((element) {
      //print("element : ${element}");
      if (element.ayNo == date.month) {
        buAykiler.add(element);
      }
    });
    return buAykiler;
  }

  @override
  void initState() {
    super.initState();
    buAyinPlanlari.clear();
    senelikTakvimOgeleriniAlFonk().then((value) {
      setState(() {
        //print("value: ${value}");
        buAyinPlanlari = value;
      });
    });
    // super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      primary: false,
      extendBody: true,
      backgroundColor: const Color(0xffDAE8FF),
      body: CustomScrollView(

        slivers: [
          SliverAppBar(
            
            backgroundColor: Colors.transparent,
            titleTextStyle: const TextStyle(
                color: Colors.black, fontSize: 19, fontWeight: FontWeight.bold),
            pinned: false,
            shadowColor: Colors.transparent,
            toolbarHeight: 47.0,
            expandedHeight: 220,
            elevation: 5.0,
            leading: IconButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const KazanimlariListele(),
                      ));
                },
                icon: const Icon(Icons.arrow_back_ios, color: Colors.white)),
            title: const AutoSizeText(
              "Bu Ay Neler Var?",
              maxFontSize: 18,

              style: TextStyle(color: Colors.white),
            ),
            actions: [
              IconButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            AkademikTakvim(senelikTakvimOgeleri),
                      ));
                },
                icon: const Icon(Icons.view_list_rounded,
                    color: Colors.white, size: 30),
              )
            ],
            flexibleSpace: FlexibleSpaceBar(
              background: Stack(
                fit: StackFit.expand,
                children: [
                  Image.asset(
                    "assets/resimler/arkplan1.png",
                    fit: BoxFit.cover,
                  ),
                  // Bu ayki ögeler listesi tek ise ogeyi ortala
                  if (buAyinPlanlari.length == 1) ...[
                    Align(
                      child: aylikTakvimWid(context, buAyinPlanlari),
                    )
                  ] else ...[
                    aylikTakvimWid(context, buAyinPlanlari)
                  ]
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                if (senelikTakvimOgeleri[index].ayNo == 0) {
                  return takvimBaslikWid(context, senelikTakvimOgeleri[index]);
                } else {
                  return takvimWid(context, senelikTakvimOgeleri[index]);
                }
              },
              childCount: senelikTakvimOgeleri.length,
            ),
          ),
        ],
      ),
    );
  }

  aylikTakvimWid(BuildContext context, List buAyinPlanlari) {
    return ListView(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      children: List.generate(buAyinPlanlari.length, (index) {
        return Container(
          width: MediaQuery.of(context).size.width / 2.5,
          margin: const  EdgeInsets.fromLTRB(8, 80, 10, 12),
          padding: const EdgeInsets.all(0),
          decoration: BoxDecoration(
            color: Color.fromARGB(
                    buAyinPlanlari[index].renk[0].toInt(),
                    buAyinPlanlari[index].renk[1].toInt(),
                    buAyinPlanlari[index].renk[2].toInt(),
                    buAyinPlanlari[index].renk[3].toInt())
                .withOpacity(0.8),
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(
                        buAyinPlanlari[index].renk[0].toInt(),
                        buAyinPlanlari[index].renk[1].toInt(),
                        buAyinPlanlari[index].renk[2].toInt(),
                        buAyinPlanlari[index].renk[3].toInt())
                    .withOpacity(0.7), //color of shadow
                spreadRadius: 2, //spread radius
                blurRadius: 6, // blur radius
                offset: const Offset(0, 2), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 2, 0),
                    child: AutoSizeText(
                        "${buAyinPlanlari[index].tarih.substring(0, 2)}",
                        maxFontSize: 45,
                        minFontSize: 38,

                        style: const TextStyle(
                            fontSize: 45,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                  ),
                  Wrap(
                    alignment: WrapAlignment.start,
                    crossAxisAlignment: WrapCrossAlignment.start,
                    direction: Axis.vertical,
                    spacing: 0.0,
                    children: [
                      AutoSizeText(
                        "${buAyinPlanlari[index].ay}",
                        maxFontSize: 21,
                        minFontSize: 19,

                        style: const TextStyle(
                            fontSize: 21,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      AutoSizeText(
                        "${buAyinPlanlari[index].gun}",
                        maxFontSize: 15,
                        minFontSize: 14,

                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: Colors.white70),
                      ),
                    ],
                  )
                ],
              ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.fromLTRB(3, 2, 3, 3),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "${buAyinPlanlari[index].belirliGun}",
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.white, fontSize: 14),
                      maxLines: 4,
                    ),
                    Text(
                      "${buAyinPlanlari[index].aciklama}",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          color: Colors.black87,
                          fontSize: 12,
                          fontStyle: FontStyle.italic),
                      maxLines: 2,
                    ),
                  ],
                ),
              ))
            ],
          ),
        );
      }),
    );
  }

  // yıllık takvi başılıgını olusturan widget
  Widget takvimBaslikWid(BuildContext context, Takvim oge) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 1),
      child: Container(
        padding: const EdgeInsets.fromLTRB(7, 2, 7, 2),
        height: 30,
        width: MediaQuery.of(context).size.width / 2.5,
        decoration: const BoxDecoration(
          color: Color.fromRGBO(200, 200, 200, 0.2),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(0),
              topLeft: Radius.circular(18),
              bottomRight: Radius.circular(1),
              topRight: Radius.circular(18)),
        ),
        child: const Align(
          child: AutoSizeText(" Eğitim Öğretim Yılı Takvimi ",
              maxFontSize: 18,
              minFontSize: 11,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w300,
                  color: Colors.black87),
              textAlign: TextAlign.center),
        ),
      ),
    );
  }

  Widget takvimWid(BuildContext context, Takvim oge) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
      child: Container(
        padding: EdgeInsets.zero,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 4, 12, 0),
                  child: Text("${oge.tarih.substring(0, 2)}",
                      style: const TextStyle(
                          fontSize: 32,
                          fontWeight: FontWeight.bold,
                          color: Colors.black)),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 0, 12, 10),
                  child: Text("${oge.ay}",
                      style: const TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.normal,
                          color: Colors.black87)),
                ),
              ],
            ),
            Flexible(
              flex: 1,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    decoration: BoxDecoration(
                      color: Color.fromARGB(
                              oge.renk[0].toInt(),
                              oge.renk[1].toInt(),
                              oge.renk[2].toInt(),
                              oge.renk[3].toInt())
                          .withOpacity(0.7),
                      borderRadius: BorderRadius.circular(5.0),
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromARGB(
                                  oge.renk[0].toInt(),
                                  oge.renk[1].toInt(),
                                  oge.renk[2].toInt(),
                                  oge.renk[3].toInt())
                              .withOpacity(0.5), //color of shadow
                          spreadRadius: 2, //spread radius
                          blurRadius: 5, // blur radius
                          offset: const Offset(0, 0), // changes position of shadow
                        ),
                      ],
                    ),
                    child: ExpansionTile(
                      collapsedIconColor: Colors.white,
                      collapsedTextColor: Colors.white,
                      iconColor: Colors.black54,
                      textColor: Colors.white,
                      title: Text(
                        "${oge.belirliGun}",
                        style: const TextStyle(fontSize: 17),
                      ),
                      subtitle: Text("${oge.cesit}",
                          style: const TextStyle(fontSize: 12, color: Colors.black)),
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 5, 8, 9),
                          child: Text("${oge.aciklama}"),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 1, 10, 0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        "${oge.gun} ${oge.tarih}",
                        style: const TextStyle(fontSize: 11, color: Colors.black87),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
