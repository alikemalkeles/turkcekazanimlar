import 'package:flutter/material.dart';

import '../model/takvimModel.dart';



// ignore: must_be_immutable
class AkademikTakvim extends StatelessWidget {
  List<Takvim> liste;

  AkademikTakvim(this.liste, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          backgroundColor: const Color(0xffDAE8FF),
          appBar: AppBar(
            backgroundColor: const Color(0xffDAE8FF),
            title: const Text(
              "Akademik Takvim (Liste)",
              style: TextStyle(color: Colors.black, fontSize: 17),
            ),
            elevation: 0.0,
            toolbarHeight: 43,
            
            leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(
                  Icons.arrow_back_ios_outlined,
                  color: Colors.black,
                )),
          ),
          body: Container(
            decoration: const BoxDecoration(
            color:  Color(0xffDAE8FF),
            borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18.0),
            topRight: Radius.circular(0.0)
            ),
          ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 5),
              child: ListView.builder(
                  itemCount: liste.length,
                  itemBuilder: (context, int index) {
                    if (liste[index].tarih == "Liste Başlığı") {
                      return const Text(
                        "",
                        style: TextStyle(fontSize: 5),
                      );
                    } else {
                      return Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          const Icon(
                            Icons.arrow_right_sharp,
                            color: Colors.amber,
                            size: 30,
                          ),
                          Container(
                            width: 120,
                            child: Text(
                              "${liste[index].tarih}",
                              maxLines: 1,
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              "${liste[index].belirliGun}",
                              maxLines: 2,
                              style: TextStyle(
                                fontSize: 15,
                                color: Color.fromARGB(
                                        liste[index].renk[0].toInt(),
                                        liste[index].renk[1].toInt(),
                                        liste[index].renk[2].toInt(),
                                        liste[index].renk[3].toInt())
                                    .withOpacity(0.9),
                              ),
                            ),
                          )
                        ],
                      );
                    }
                  }),
            ),
          ));
    
  }
}
