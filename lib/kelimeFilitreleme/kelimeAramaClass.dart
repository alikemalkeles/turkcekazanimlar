

class Kelime {
  // basi kelime arama fonksiyonu
  basitKelimeArama(String ara, var KelimeListemiz) {
    var tumKelimeler_ = KelimeListemiz;
    var aranan = ara;
    var anlamli = [];

    //liste içindeki her bir liste grubu
    for (var kelime in tumKelimeler_) {
      var sorgu = [];

      for (var k in kelime.split("")) {
        if (aranan.contains(k)) {
          sorgu.add(1);
        } else {
          sorgu.add(0);
        }
      }

      if (sorgu.contains(0) == false && kelime.toString().length > 1) {
        anlamli.add(kelime);
      }
    }
    // dongu sonu
    return anlamli;
  } // basit kelime arama fonk

  // detayliKelimeArama
  detayliKelimeArama(String ara, var KelimeListemiz2) {
    //
    var KelimeListemiz2_ = KelimeListemiz2;
    var arananKelime = ara;
    var anlamliKelimeler = basitKelimeArama(arananKelime, KelimeListemiz2_);

    var bulunanOgeler = [];

    for (var anlamliOge in anlamliKelimeler) {
      // gecici sorgu alanı sartllar
      List sor = [];
      // aranan kelimeyi sözlük yapısına göre dizme
      var arananSozluk = {
        for (var oge in arananKelime.split(""))
          oge: oge.allMatches(arananKelime).length
      };
      var anlamliSozluk = {
        for (var oge in anlamliOge.split(""))
          oge: oge.allMatches(anlamliOge).length
      };
      //print(anlamliSozluk);
      // aranan kelime ve donguden donen kelimelerin içeriklerini karsılastırma
      for (var veri in anlamliSozluk.entries) {
        if (veri.value <= arananSozluk[veri.key]) {
          //
          sor.add(1);
        } else {
          sor.add(0);
        }
      }

      //print("sor değişkeni $sor");
      if (sor.contains(0)) {
        continue;
      } else {
        bulunanOgeler.add(anlamliOge);
      }
    }
    return bulunanOgeler;
  }
// }// ana sınıf kapama parantezi
}