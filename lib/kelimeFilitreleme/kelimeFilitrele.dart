// import 'dart:html';
//import 'dart:ui';

import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import '../kazanimlar/kazanimlariListele.dart';
import 'kelimeAramaClass.dart';
import "package:flutter/services.dart" show rootBundle;
import "dart:async";
import 'package:fluttertoast/fluttertoast.dart';


class KelimeArama extends StatefulWidget {
  
  @override
  _KelimeArama createState() => _KelimeArama();
  
}

class _KelimeArama extends State<KelimeArama> {
  // dosya okuma
  List<dynamic> kelimeListesi = [];
  Future loadData() async {
    var loadedData = await rootBundle.loadString("assets/kaynaklar/kelimeListesi.json");
    print("loaded data: ${loadedData.length}");
    Map<String, dynamic> jsonData =  await json.decode(loadedData) ?? {};
    print("loaded data: ${loadedData.length}");
    kelimeListesi = jsonData["kelimeler"];
    print("kelime listesi alındı");
  }

  //Arama yapıldığında bututonu devre dışı bırak sonuclar gelince aktif et

  // switch button varsayılan değişken
  bool status = false;
  // sonuc sayac degiskeni
  var sayac = 0;
  TextEditingController textController = TextEditingController();
  // arama sonucunun indexlenecegi liste listBuilder fonksiyonu tarafından kullanılacak
  List<dynamic> sonuclar = [];

  void bosListe() {
    sonuclar.clear();
    sayac = 0;
  }
  //TODO: yapapacaklar

  void kelimeAra() async {
    print("data degiskeni durumu ${kelimeListesi.length}");

    if (textController.text.isNotEmpty) {
      //sonuclar.add(data_.length);
      // switch button durumuna göre rama yapma
      if (status == false) {
        print("text bos degil");

        // listeyi harf boyutlarına göre sıralama
        kelimeListesi.sort((a, b) => a.length.compareTo(b.length));
        // sonuc degiskenini yeni deger eklemeden icin
        sonuclar.clear();
        // basit arama
        var aramaSonucu =
            Kelime().basitKelimeArama(textController.text, kelimeListesi);
        // arama sonucunu küme yapma
        Set<String> kume = {};
        for (var deger in aramaSonucu) {
          kume.add(deger);
        }
        // arama sonucunu olcup sayac degiskenine atama
        sayac = kume.toList().length;

        sonuclar.addAll(kume.toList());
      } else {
       
        // listeyi harf boyutlarına göre sıralama
        kelimeListesi.sort((a, b) => a.length.compareTo(b.length));
        // sonuc degiskenini yeni deger eklemeden icin
        sonuclar.clear();
        // basit arama
        var aramaSonucu =
            Kelime().detayliKelimeArama(textController.text, kelimeListesi);
        // arama sonucunu küme yapma
        Set<String> kume = {};
        for (var deger in aramaSonucu) {
          kume.add(deger);
        }
        // arama sonucunu olcup sayac degiskenine atama
        sayac = kume.toList().length;

        sonuclar.addAll(kume.toList());
      }
    } else {
      print("text edit boştur");
    }
    // eger sonunuc yok ise
    if (sonuclar.length == 0) {
      toastMesaji("Öge bulunamadı");
    } else {
      Future.delayed(const Duration(milliseconds: 400), () {
        FocusScope.of(context).unfocus();
      });
    }
  }

  //toast mesajı
  void toastMesaji(String mesaj) {
    Fluttertoast.showToast(
      msg: "${mesaj}",
      fontSize: 18,
      backgroundColor: Colors.amberAccent,
      textColor: Colors.black,
      gravity: ToastGravity.TOP,
    );
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // arkaplan rengi
      backgroundColor: const Color(0xffDAE8FF),
      appBar: AppBar(
        backgroundColor: const Color(0xffDAE8FF),
        elevation: 0.0,
        titleSpacing: 0.0,
        toolbarHeight: 70,
        leadingWidth: 42,
        centerTitle: true,
        title: Container(
          height: 40,
          margin: const EdgeInsets.fromLTRB(1, 0, 0, 0),
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  style: const TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                  controller: textController,
                  keyboardType: TextInputType.text,
                  maxLines: 1,
                  autofocus: true,
                  cursorColor: Colors.black,
                  cursorWidth: 2.0,
                  textCapitalization: TextCapitalization.words,
                  autocorrect: false,
                  textInputAction: TextInputAction.go,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.fromLTRB(7, 0, 0, 0),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Sözcük türetmek için harf giriniz",
                    hintStyle:
                        const TextStyle(color: Colors.black54, fontSize: 15),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          textController.clear();
                          bosListe();
                        });
                      },
                      icon: const Icon(Icons.clear),
                      color: Colors.red,
                      padding: const EdgeInsets.symmetric(),
                    ),
                    border: const OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.all(Radius.circular(22.0))),
                  ),
                ),
              ),
              IconButton(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(0),
                icon: const Icon(Icons.search_rounded),
                color: Colors.black87,
                iconSize: 32,
                onPressed: () {
                  setState(() {
                    kelimeAra();
                  });
                },
              ),
            ],
          ),
        ),
        leading: Align(
          alignment: Alignment.center,
          child: IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const KazanimlariListele(),
                    ));
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
                size: 30,
              )),
        ),
        bottom: PreferredSize(
          preferredSize: Size(MediaQuery.of(context).size.width, 25),
          child: Container(
            margin: const EdgeInsets.fromLTRB(18, 0, 20, 0),
            child: Row(
              children: [
                const Text("Harf kısıtlaması ",
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: 14,
                        fontWeight: FontWeight.w300)),
                Switch(
                  activeColor: Colors.white,
                  activeTrackColor: Colors.green[300],
                  inactiveTrackColor:  const Color(0xffDAE8FF),
                  inactiveThumbColor: Colors.grey,
                  value: status,
                  onChanged: (value) {
                    print("Value: $value");
                    setState(
                      () {
                        status = value;
                      },
                    );
                  },
                ),
                // switch button ile sayac arasına bosluk ekleme
                const Spacer(),
                Chip(
                  elevation: 1,
                  backgroundColor: const Color(0xffDAE8FF),
                  side: const  BorderSide(width: 0, color: Color(0xffDAE8FF)),
                  label: Text(
                    "Sonuç: ${sayac}",
                    style: const TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.w400),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          color: Color(0xffDAE8FF),//Color.fromARGB(211, 114, 159, 209), //Color.fromARGB(72, 219, 220, 255),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(0.0), topLeft: Radius.circular(18.0)),
        ),
        padding: const EdgeInsets.fromLTRB(1, 6, 1, 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Scrollbar(
                thickness: 7, //width of scrollbar
                radius: const Radius.circular(10), //corner radius of scrollbar
                scrollbarOrientation: ScrollbarOrientation.right,
                child: GridView.count(
                  padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                  crossAxisCount: 2,
                  scrollDirection: Axis.vertical,
                  // shrinkWrap: true,
                  crossAxisSpacing: 7.0,
                  // mainAxisSpacing: 4.0,
                  childAspectRatio: 5,
                  children: List.generate(sonuclar.length, (index) {
                    return Align(
                        alignment: Alignment.centerLeft,
                        child: Chip(
                          backgroundColor: Colors.white70,
                          padding: const EdgeInsets.fromLTRB(0, 0, 0, 2),
                          elevation: 2,
                          labelPadding: const EdgeInsets.fromLTRB(0, 0, 8, 0),
                          label: AutoSizeText(
                            "${sonuclar[index]}",
                            maxFontSize: 15,
                            minFontSize: 10,
                            style: const TextStyle(
                                color: Colors.black, fontSize: 15),
                            maxLines: 2,
                            softWrap: true,
                            overflow: TextOverflow.clip,
                          ),
                          avatar: CircleAvatar(
                            backgroundColor: Colors.white70,
                            child: Text(
                              "${sonuclar[index].toString().length}",
                              style: const TextStyle(
                                  color: Colors.amber,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ));
                  }),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
