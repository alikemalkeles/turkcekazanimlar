import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:week_of_year/week_of_year.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:blurrycontainer/blurrycontainer.dart';

import '../model/modelKurs.dart';
import 'kazanimlariListele.dart';

class KursKazanimGoruntule extends StatefulWidget {
  List<ModelKurs>
      kursKazanimi; // Kazanimlari listele dosyasından gelecek olan arguman
  List<String> tumHaftalarinAdi = [];

  KursKazanimGoruntule(this.kursKazanimi);

  @override
  _KursKazanimGoruntuleState createState() =>
      _KursKazanimGoruntuleState(kursKazanimi);
}

class _KursKazanimGoruntuleState extends State<KursKazanimGoruntule> {
  List<ModelKurs> kursKazanimi; // Kazanimlari listele dosyasından gelecek olan arguman
  List<List> tumHaftalar = [];

  _KursKazanimGoruntuleState(this.kursKazanimi);

  final PageController _controller = PageController();
  String title = "__";

  Color? scaffoldColor = Colors.teal[600];
  Color? containerColor = Colors.teal[400];

  @override
  void initState() {
    tumHaftalar_();
    // Hafta belirlenip arguman olarak sayfa gecis kontrol fonksiyonuna iletilir.
    haftaNumarasiAlFonk().then((value) {
      print("tüm haftalar listesindeki öge sayısı ${tumHaftalar.length}");
      didChanceDependencies(value);
      setState(() {
      title =
          "${kursKazanimi[0].ders} ${kursKazanimi[0].sinif}. Sınıf DYK Planı";
       });
    });
    super.initState();
    
  }

  void didChanceDependencies(int sayfa) {
    // Widget build metodunu dinleyip build işlemi bittikten sonra sayfa  kontroller üzerinden hafta numarasına göre
    // içinde bulunulan haftaya gecis yap

    WidgetsBinding.instance.addPostFrameCallback((_) {
      //print("dinliyor: ${_controller.hasClients}");
        if (_controller.hasClients) {
            _controller.jumpToPage(sayfa);
            // _controller.animateToPage(sayfa,
            //     duration: const Duration(milliseconds: 100), curve: Curves.bounceOut);
         }
    });
    super.didChangeDependencies();
  }

  Future haftaNumarasiAlFonk() async {
    // Yıl içindeki hangi haftada olduğumuzu belirle
    final date =  DateTime.now();
    print("hafta no: ${date.weekOfYear}");
    // hafta numarasının bulunduğu kazanımın sirasını ders kazanımları listesi içinde belirle
    int result = kursKazanimi.indexWhere((oge) =>
        oge.haftaNo ==
        date.weekOfYear); // ogenin dizinini verir. Oge yok ise -1 doner
    print("Kazanım index result: ${result}");
    // eger result 40 tan büyük ise hep 40. indexi döndür
    int toplamKazanimSayisi = kursKazanimi.length - 1 ?? 0;
    print("toplam KURS kazanımı sayısı: ${toplamKazanimSayisi}");
    if (result <= toplamKazanimSayisi && result >= 0) {
      print("buradasın ");
      return result;
    } else {
      return toplamKazanimSayisi;
    }
  }

  //BottomSheet üzerinden hafta seçimi
  tumHaftalar_() {
    for (var oge in kursKazanimi) {
      tumHaftalar.add([oge.hafta, oge.konu]);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.miniEndTop,
        extendBodyBehindAppBar: true,
        extendBody: true,
        backgroundColor: scaffoldColor, // Colors.indigo[900],
        appBar: AppBar(
          toolbarHeight: MediaQuery.of(context).size.height / 15.6,
          leadingWidth: 32,
          leading: IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => KazanimlariListele(),
                    ));
              },
              icon: const Icon(
                Icons.arrow_back_ios_outlined,
                color: Colors.white,
              )),
          actions: [
            IconButton(
                hoverColor: Colors.transparent,
                onPressed: () {
                  BottomSheetPanel(context);
                },
                icon: const Icon(
                  Icons.today_outlined,
                  color: Colors.white,
                  size: 26,
                ))
          ],
          title: AutoSizeText(
            "${title}",
            maxFontSize: 18,
            minFontSize: 14,
            maxLines: 1,
            style: const TextStyle(
                fontSize: 17, fontWeight: FontWeight.w600, color: Colors.white),
          ),
          backgroundColor: scaffoldColor,
          centerTitle: true,
          shadowColor: Colors.black54,
          elevation: 0.0,
        ),
        body: SafeArea(
          child: PageView.builder(
            scrollDirection: Axis.horizontal,
            controller: _controller,
            itemCount: kursKazanimi.length,
            itemBuilder: (context, index) {
              // kazanımlarda konu ara tatil değilse kazımları göster aksi halde tatil olarak belirt
              if (kursKazanimi[index].konu == "Ara Tatil") {
                return tatilWidget(context, kursKazanimi[index]);
              } else if (kursKazanimi[index].konu == "Yarıyıl Tatili") {
                return tatilWidget(context, kursKazanimi[index]);
              } else if (kursKazanimi[index].konu == "Yaz Tatili") {
                return tatilWidget(context, kursKazanimi[index]);
              } else {
                return pageWidget(context, kursKazanimi[index]);
              }
            },
          ),
        ),
      );
    
  }

  // hızlıca belirli haftaya gitmak için oluşturulmus bottom panel
  void BottomSheetPanel(context) {
    showModalBottomSheet(
        context: context,
        useRootNavigator: true,
        backgroundColor: Colors.transparent,
        barrierColor: Colors.black.withAlpha(1),
        elevation: 0,
        isScrollControlled: true,
        constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height / 1.30),
        builder: (context) => BlurryContainer(
              padding: const EdgeInsets.all(0.0),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
                bottomRight: Radius.circular(0),
                bottomLeft: Radius.circular(0),
              ),
              blur: 8,
              child: Container(
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Colors.white.withOpacity(0.5),
                      Colors.black87.withOpacity(0.2)
                    ]),
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(0),
                      bottomLeft: Radius.circular(0),
                    )),
                child: SingleChildScrollView(
                  physics: ScrollPhysics(),
                  child: Column(
                    children: [
                      Chip(
                          elevation: 5,
                          backgroundColor: Colors.white.withOpacity(0.7),
                          label: const Text(
                            "     Hafta Seçiniz     ",
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w600,
                                color: Colors.black),
                          )),
                      ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: tumHaftalar.length,
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              margin: const EdgeInsets.fromLTRB(4, 5, 4, 5),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8.0),
                                gradient: LinearGradient(
                                    stops: [0.02, 0.02],
                                    colors: [scaffoldColor!, Colors.white]),
                                boxShadow: [
                                  BoxShadow(
                                    color: scaffoldColor
                                        !.withOpacity(0.6), //color of shadow
                                    spreadRadius: 1, //spread radius
                                    blurRadius: 2, // blur radius
                                    offset: const Offset(
                                        0, 2), // changes position of shadow
                                  ),
                                  //you can set more BoxShadow() here
                                ],
                              ),
                              child: ListTile(
                                dense: true,
                                leading: Icon(
                                  Icons.today_outlined,
                                  color: scaffoldColor,
                                  size: 32,
                                ),
                                title: Text(
                                  "${tumHaftalar[index][0]}",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      color: scaffoldColor),
                                ),
                                subtitle: Text(
                                  "${tumHaftalar[index][1]}",
                                  style: const TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400),
                                  maxLines: 2,
                                ),
                                onTap: () {
                                  int result_ = kursKazanimi.indexWhere((oge) =>
                                      oge.hafta == tumHaftalar[index][0]);
                                  print("hafta sırası: ${result_}");
                                  Navigator.pop(context);
                                  //_controller.animateToPage(result_, duration: Duration(milliseconds: 100), curve: Curves.slowMiddle);
                                  _controller.animateToPage(result_,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.slowMiddle);
                                  // _controller.jumpToPage(result_);
                                },
                              ),
                            );
                          })
                    ],
                  ),
                ),
              ),
            ));
  }

  // Kazanimin içeriğini listeleyen widget, arguman olarak kazanımı alır ( Plan)
  Widget pageWidget(BuildContext context, ModelKurs plan) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5, 0, 5, 5),
      child: Container(
        color: scaffoldColor,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(0, 1, 0, 4),
              child: Text(
                "${plan.hafta}",
                maxLines: 1,
                style: const TextStyle(
                    color: Colors.amberAccent,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color:
                      containerColor, //Color.fromARGB(211, 114, 159, 209), //Color.fromARGB(72, 219, 220, 255),
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(14.0),
                      topLeft: Radius.circular(14.0),
                      bottomLeft: Radius.circular(14.0),
                      bottomRight: Radius.circular(14.0)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black54.withOpacity(0.4), //color of shadow
                      spreadRadius: 1, //spread radius
                      blurRadius: 5, // blur radius
                      offset: Offset(0, 2), // changes position of shadow
                      //first paramerter of offset is left-right
                      //second parameter is top to down
                    ),
                    //you can set more BoxShadow() here
                  ],
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(7.0),
                        child: Text(
                          "${plan.konu}",
                          style: const TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                          maxLines: 2,
                        ),
                      ),
                      cizgiWidget(context),
                      for (var i in plan.kazanim) ...[
                        textWidget(context, i),
                      ],
                      const Padding(
                        padding: EdgeInsets.fromLTRB(14, 7, 2, 3),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Wrap(
                            spacing: 4.0,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children:  [
                              Icon(
                                Icons.extension_outlined,
                                color: Colors.amberAccent,
                                size: 22,
                              ),
                              Text(
                                "ETKİNLİK",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.amberAccent),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(18, 7, 2, 3),
                        child: Align(
                            alignment: Alignment.bottomLeft,
                            child: Text("${plan.etkinlik}")),
                      ),
                      const Padding(
                        padding: EdgeInsets.fromLTRB(14, 7, 2, 3),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Wrap(
                            spacing: 4.0,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children:  [
                              Icon(
                                Icons.help_outline_outlined,
                                color: Colors.amberAccent,
                                size: 22,
                              ),
                              Text(
                                "AÇIKLAMA",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.amberAccent),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(18, 7, 2, 3),
                        child: Align(
                            alignment: Alignment.bottomLeft,
                            child: Text("${plan.aciklama}")),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Kazanimin içeriğini listeleyen widget, arguman olarak kazanımı alır ( Plan)
  Widget tatilWidget(BuildContext context, ModelKurs plan) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5, 0, 5, 5),
      child: Container(
        color: Colors.transparent,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
              child: Text(
                "${plan.hafta}",
                maxLines: 1,
                style: const TextStyle(
                    color: Colors.orangeAccent,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  image: const DecorationImage(
                      image: AssetImage("assets/resimler/tatil1.png"),
                      fit: BoxFit
                          .cover), //Color.fromARGB(211, 114, 159, 209), //Color.fromARGB(72, 219, 220, 255),
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(14.0),
                      topLeft: Radius.circular(14.0),
                      bottomLeft: Radius.circular(14.0),
                      bottomRight: Radius.circular(14.0)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black54.withOpacity(0.4), //color of shadow
                      spreadRadius: 1, //spread radius
                      blurRadius: 5, // blur radius
                      offset: Offset(0, 2), // changes position of shadow
                      //first paramerter of offset is left-right
                      //second parameter is top to down
                    ),
                    //you can set more BoxShadow() here
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: BlurryContainer(
                        padding: const EdgeInsets.fromLTRB(28, 5, 28, 5),
                        color: containerColor!.withOpacity(0.9),
                        blur: 5,
                        elevation: 0,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        child: Text(
                          " ${plan.konu}        ",
                          style: const TextStyle(
                              fontSize: 22,
                              color: Colors.white,
                              fontWeight: FontWeight.w800),
                          textAlign: TextAlign.center,
                          maxLines: 1,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(5, 2, 5, 6),
                      child: BlurryContainer(
                        padding: const EdgeInsets.fromLTRB(20, 7, 20, 7),
                        color: Colors.white70.withOpacity(0.0),
                        blur: 0,
                        elevation: 0,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(25)),
                        child: const Text(
                          "Tatilin Keyfini Çıkar",
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.white,
                              fontSize: 23),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // cizgi oluşturmak icin kullanılan widget
  Widget cizgiWidget(BuildContext context) {
    return Container(
      height: 1.0,
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
    );
  }

  // Kazanım sayfasında Kazanımı yazdıran widget döngü üzerinde kullanılmakta
  Widget textWidget(BuildContext context, String icerik) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15.0, 3, 6.0, 0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          "● ${icerik}",
          textAlign: TextAlign.left,
          style: const TextStyle(color: Colors.white, fontSize: 17),
        ),
      ),
    );
  }
}
