import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:week_of_year/week_of_year.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:turkish/turkish.dart';

import '../model/modelDers.dart';
import 'kazanimlariListele.dart';

// ignore: must_be_immutable
class DersKazanimGoruntule extends StatefulWidget {
  List<ModelDers>
      dersKazanimi; // Kazanimlari listele dosyasından gelecek olan arguman
  List<String> tumHaftalarinAdi = [];

  DersKazanimGoruntule(this.dersKazanimi, {Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api, no_logic_in_create_state
  _DersKazanimGoruntuleState createState() => _DersKazanimGoruntuleState(dersKazanimi);
}

class _DersKazanimGoruntuleState extends State<DersKazanimGoruntule> {
  List<ModelDers>
      dersKazanimi; // Kazanimlari listele dosyasından gelecek olan arguman
  List<List> tumHaftalar = [];

  _DersKazanimGoruntuleState(this.dersKazanimi);

  final PageController _controller = PageController();
  String title = "__";

  Color? scaffoldColor = Colors.indigo[600];
  Color? containerColor = Colors.indigo[400];

  @override
  void initState() {
    tumHaftalar_();
    // Hafta belirlenip arguman olarak sayfa gecis kontrol fonksiyonuna iletilir.
    haftaNumarasiAlFonk().then((value) {
      didChanceDependencies(value);
    });
    super.initState();
    setState(() {
      // Ders okuma veya yazma becerileri ise yayınevini gösterme
      title = dersKazanimi[0].yayinEvi.length == 0
          ? "${dersKazanimi[0].ders}"
          : "${dersKazanimi[0].ders} - ${dersKazanimi[0].yayinEvi}";
    });
  }

  void didChanceDependencies(int sayfa) {
    // Widget build metodunu dinleyip build işlemi bittikten sonra sayfa  kontroller üzerinden hafta numarasına göre
    // içinde bulunulan haftaya gecis yap

    WidgetsBinding.instance.addPostFrameCallback((_) {
      //print("dinliyor: ${_controller.hasClients}");
      if (_controller.hasClients) {
        _controller.jumpToPage(sayfa);
      }
    });
    super.didChangeDependencies();
  }

  Future haftaNumarasiAlFonk() async {
    // Yıl içindeki hangi haftada olduğumuzu belirle
    final date =  DateTime.now();
    print("hafta no: ${date.weekOfYear}");
    // hafta numarasının bulunduğu kazanımın sirasını ders kazanımları listesi içinde belirle
    int result = dersKazanimi.indexWhere((oge) => oge.haftaNo == date.weekOfYear); // ogenin dizinini verir. Oge yok ise -1 doner
    print("Kazanım index: ${result}");   // date.weekOfYear
    // eger result 40 tan büyük ise hep 40. indexi döndür
    // Bir dönemde ortalama 41 haftalık kazanım oluyor. Tam olarak bu kazanımları belirlemek için 
    // toplam kazınmların sayısını belirle
    int toplamKazanimSayisi = dersKazanimi.length - 1 ?? 0;
    print("Toplam ders kazanım sayısı: ${toplamKazanimSayisi}");
    if (result <= toplamKazanimSayisi && result >= 0) {
      print("buradasın ${dersKazanimi[toplamKazanimSayisi].haftaNo}");
      return result;
    } else {
      return toplamKazanimSayisi;
    }
  }

  Future tumHaftalar_() async {
    for (var oge in dersKazanimi) {
      tumHaftalar.add([oge.hafta, oge.konu]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.miniEndTop,
        extendBodyBehindAppBar: true,
        extendBody: true,
        backgroundColor: scaffoldColor, // Colors.indigo[900],
        appBar: AppBar(
          toolbarHeight: MediaQuery.of(context).size.height / 15.6,
          leadingWidth: 32,
          leading: IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const KazanimlariListele(),
                    ));
              },
              icon: const Icon(
                Icons.arrow_back_ios_outlined,
                color: Colors.white,
              )),
          actions: [
            IconButton(
                hoverColor: Colors.transparent,
                onPressed: () {
                  BottomSheetPanel(context);
                },
                icon: const Icon(
                  Icons.today_outlined,
                  color: Colors.white,
                  size: 26,
                ))
          ],
          title: AutoSizeText(
            title,
            maxFontSize: 18,
            minFontSize: 14,
            maxLines: 1,
            style: const TextStyle(
                fontSize: 17, fontWeight: FontWeight.w600, color: Colors.white),
          ),
          backgroundColor: scaffoldColor,
          centerTitle: true,
          shadowColor: Colors.black54,
          elevation: 0.0,
        ),
        body: SafeArea(
          child: PageView.builder(
            scrollDirection: Axis.horizontal,
            controller: _controller,
            itemCount: dersKazanimi.length,
            itemBuilder: (context, index) {
              // kazanımlarda konu ara tatil değilse kazımları göster aksihalde tatil olarak belirt.
              
              if (dersKazanimi[index].konu != "Tatil") {
                return kazanimWidget(context, dersKazanimi[index]);
              } else {
                return tatilWidget(context, dersKazanimi[index]);
              }
            },
          ),
        ),
      );
  
  }

  // hızlıca belirli haftaya gitmak için oluşturulmus bottom panel
  // ignore: non_constant_identifier_names
  void BottomSheetPanel(context) {
    showModalBottomSheet(
        context: context,
        useRootNavigator: true,
        backgroundColor: Colors.transparent,
        barrierColor: Colors.black.withAlpha(1),
        elevation: 0,
        isScrollControlled: true,
        constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height / 1.30),
        builder: (context) => BlurryContainer(
              padding: const EdgeInsets.all(0.0),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
                bottomRight: Radius.circular(0),
                bottomLeft: Radius.circular(0),
              ),
              blur: 8,
              child: Container(
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Colors.white.withOpacity(0.5),
                      Colors.black87.withOpacity(0.2)
                    ]),
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(0),
                      bottomLeft: Radius.circular(0),
                    )),
                child: SingleChildScrollView(
                  physics: const ScrollPhysics(),
                  child: Column(
                    children: [
                      Chip(
                          elevation: 5,
                          backgroundColor: Colors.white.withOpacity(0.7),
                          label: const Text(
                            "     Hafta Seçiniz     ",
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w600,
                                color: Colors.black),
                          )),
                      ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: tumHaftalar.length,
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              margin: const EdgeInsets.fromLTRB(4, 5, 4, 5),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8.0),
                                gradient: LinearGradient(
                                    stops: const [0.02, 0.02],
                                    colors: [scaffoldColor!, Colors.white]),
                                boxShadow: [
                                  BoxShadow(
                                    color: scaffoldColor!
                                        .withOpacity(0.6), //color of shadow
                                    spreadRadius: 1, //spread radius
                                    blurRadius: 2, // blur radius
                                    offset: const Offset(
                                        0, 2), // changes position of shadow
                                  ),
                                  //you can set more BoxShadow() here
                                ],
                              ),
                              child: ListTile(
                                dense: true,
                                leading: Icon(
                                  Icons.today_outlined,
                                  color: scaffoldColor,
                                  size: 32,
                                ),
                                title: Text(
                                  "${tumHaftalar[index][0]}",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      color: scaffoldColor),
                                ),
                                subtitle: Text(
                                  turkish.toUpperCase("${tumHaftalar[index][1]}"),
                                  style: const TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400),
                                  maxLines: 2,
                                ),
                                onTap: () {
                                  int result_ = dersKazanimi.indexWhere((oge) =>
                                      oge.hafta == tumHaftalar[index][0]);
                                  // print("hafta sırası: ${result_}");
                                  Navigator.pop(context);
                                  _controller.animateToPage(result_,
                                      duration: const Duration(milliseconds: 500),
                                      curve: Curves.slowMiddle);
                                  //_controller.animateToPage(result_, duration: Duration(milliseconds: 100), curve: Curves.slowMiddle);
                                  //_controller.jumpToPage(result_);
                                },
                              ),
                            );
                          })
                    ],
                  ),
                ),
              ),
            ));
  }

  // Kazanimin içeriğini listeleyen widget, arguman olarak kazanımı alır ( Plan ders)
  Widget kazanimWidget(BuildContext context, ModelDers plan) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5, 0, 5, 5),
      child: Container(
        color: scaffoldColor,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 1, 0, 4),
              child: Text(
                plan.hafta,
                maxLines: 1,
                style: const TextStyle(
                    color: Colors.amberAccent,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color:
                      containerColor, //Color.fromARGB(211, 114, 159, 209), //Color.fromARGB(72, 219, 220, 255),
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(14.0),
                      topLeft: Radius.circular(14.0),
                      bottomLeft: Radius.circular(14.0),
                      bottomRight: Radius.circular(14.0)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black54.withOpacity(0.4), //color of shadow
                      spreadRadius: 1, //spread radius
                      blurRadius: 5, // blur radius
                      offset: const Offset(0, 2), // changes position of shadow
                      //first paramerter of offset is left-right
                      //second parameter is top to down
                    ),
                    //you can set more BoxShadow() here
                  ],
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      // Eger icerik okuma veya yazma becerisi değilse içeriği dogrudan görüntüle
                      if (plan.yayinEvi.isNotEmpty) ...[
                        Padding(
                          padding: const EdgeInsets.all(7.0),
                          child: Text(
                            turkish.toUpperCase(plan.konu),
                            style: const TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.center,
                            maxLines: 2,
                          ),
                        ),
                        cizgiWidget(context),
                        wrapWidget_(context, "TEMA"),
                        textWidget(context, turkish.toUpperCase(plan.tema)),
                        wrapWidget_(context, "OKUMA"),
                        for (var okuma in plan.okuma) ...[
                          textWidget(context, okuma)
                        ],
                        wrapWidget_(context, "DİNLEME"),
                        for (var dinleme in plan.dinleme) ...[
                          textWidget(context, dinleme)
                        ],
                        wrapWidget_(context, "KONUŞMA"),
                        for (var konusma in plan.konusma) ...[
                          textWidget(context, konusma)
                        ],
                        wrapWidget_(context, "YAZMA"),
                        for (var yazma in plan.yazma) ...[
                          textWidget(context, yazma)
                        ],
                        wrapWidget_(context, "DİL BİLGİSİ"),
                        for (var dilBilgisi in plan.dilBilgisi) ...[
                          textWidget(context, dilBilgisi)
                        ],
                        wrapWidget_(context, "ETKİNLİK"),
                        for (var etkinlik in plan.etkinlik) ...[
                          textWidget(context, etkinlik)
                        ],
                        wrapWidget_(context, "AÇIKLAMA"),
                        textWidget(context, plan.aciklama[0]),
                        const Text("\n\n")
                      ] else ...[
                        // Kazanım okuma veya yazma becerisi ise kısıtlı içerik olacak
                        Padding(
                          padding: const EdgeInsets.all(7.0),
                          child: Text(
                            plan.konu,
                            style: const TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.center,
                            maxLines: 2,
                          ),
                        ),
                        cizgiWidget(context),
                        wrapWidget_(context, "OKUMA"),
                        for (var okuma in plan.okuma) ...[
                          textWidget(context, okuma)
                        ],
                        wrapWidget_(context, "ETKİNLİK"),
                        for (var etkinlik in plan.etkinlik) ...[
                          textWidget(context, etkinlik)
                        ],
                        wrapWidget_(context, "AÇIKLAMA"),
                        textWidget(context, plan.aciklama[0]),
                        const Text("\n\n")
                      ]
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Kazanimin içeriğini listeleyen widget, arguman olarak kazanımı alır ( Plan)
  Widget tatilWidget(BuildContext context, ModelDers plan) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5, 0, 5, 5),
      child: Container(
        color: Colors.transparent,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 4),
              child: Text(
                plan.hafta,
                maxLines: 1,
                style: const TextStyle(
                    color: Colors.orangeAccent,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  image: const DecorationImage(
                      image: AssetImage("assets/resimler/tatil1.png"),
                      fit: BoxFit
                          .cover), //Color.fromARGB(211, 114, 159, 209), //Color.fromARGB(72, 219, 220, 255),
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(14.0),
                      topLeft: Radius.circular(14.0),
                      bottomLeft: Radius.circular(14.0),
                      bottomRight: Radius.circular(14.0)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black54.withOpacity(0.4), //color of shadow
                      spreadRadius: 1, //spread radius
                      blurRadius: 5, // blur radius
                      offset:const  Offset(0, 2), // changes position of shadow
                      //first paramerter of offset is left-right
                      //second parameter is top to down
                    ),
                    //you can set more BoxShadow() here
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: BlurryContainer(
                        padding: const EdgeInsets.fromLTRB(28, 5, 28, 5),
                        color: containerColor!.withOpacity(0.9),
                        blur: 5,
                        elevation: 0,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        child: Text(
                          plan.tema,
                          style: const TextStyle(
                              fontSize: 22,
                              color: Colors.white,
                              fontWeight: FontWeight.w800),
                          textAlign: TextAlign.center,
                          maxLines: 1,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(5, 2, 5, 6),
                      child: BlurryContainer(
                        padding: const EdgeInsets.fromLTRB(20, 7, 20, 7),
                        color: Colors.white70.withOpacity(0.0),
                        blur: 0,
                        elevation: 0,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(25)),
                        child: const Text(
                          "Tatilin Keyfini Çıkar",
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.white,
                              fontSize: 23),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // cizgi oluşturmak icin kullanılan widget
  Widget cizgiWidget(BuildContext context) {
    return Container(
      height: 1.0,
      color: Colors.white,
      margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
    );
  }

  Widget wrapWidget_(BuildContext context, String icerik) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        margin: const EdgeInsets.fromLTRB(10, 2.0, 2, 2.0),
        padding: const EdgeInsets.fromLTRB(0.0, 2.0, 2, 0.0),
        height: 31,
        width: MediaQuery.of(context).size.width / 2.5,
        decoration: BoxDecoration(
            color: containerColor,
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(0.0),
              topLeft: Radius.circular(0.0),
              bottomRight: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
            )),
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          spacing: 4.0,
          children: [
            if (icerik == "TEMA") ...[
              const Padding(
                padding: EdgeInsets.fromLTRB(5.0, 4, 2, 4),
                child: Icon(
                  Icons.bookmark_border_outlined,
                  color: Colors.yellowAccent,
                  size: 22,
                ),
              ),
              Text(
                icerik,
                style: const TextStyle(
                  color: Colors.yellowAccent,
                  fontSize: 16,
                ),
              )
            ] else if (icerik == "OKUMA") ...[
              const Padding(
                padding: EdgeInsets.fromLTRB(5.0, 4, 2, 4),
                child: Icon(
                  Icons.local_library_outlined,
                  color: Colors.yellowAccent,
                  size: 21,
                ),
              ),
              Text(
                icerik,
                style: const TextStyle(
                  color: Colors.yellowAccent,
                  fontSize: 16,
                ),
              )
            ] else if (icerik == "DİNLEME") ...[
              const Padding(
                padding: EdgeInsets.fromLTRB(5.0, 4, 2, 4),
                child: Icon(
                  Icons.volume_up_outlined,
                  color: Colors.yellowAccent,
                  size: 21,
                ),
              ),
              Text(
                icerik,
                style: const TextStyle(
                  color: Colors.yellowAccent,
                  fontSize: 16,
                ),
              )
            ] else if (icerik == "KONUŞMA") ...[
              const Padding(
                padding: EdgeInsets.fromLTRB(5.0, 4, 2, 4),
                child: Icon(
                  Icons.record_voice_over_outlined,
                  color: Colors.yellowAccent,
                  size: 21,
                ),
              ),
              Text(
                icerik,
                style: const TextStyle(
                  color: Colors.yellowAccent,
                  fontSize: 16,
                ),
              )
            ] else if (icerik == "YAZMA") ...[
              const Padding(
                padding: EdgeInsets.fromLTRB(5.0, 4, 2, 4),
                child: Icon(
                  Icons.drive_file_rename_outline,
                  color: Colors.yellowAccent,
                  size: 21,
                ),
              ),
              Text(
                icerik,
                style: const TextStyle(
                  color: Colors.yellowAccent,
                  fontSize: 16,
                ),
              )
            ] else if (icerik == "DİL BİLGİSİ") ...[
              const Padding(
                padding: EdgeInsets.fromLTRB(5.0, 4, 2, 4),
                child: Icon(
                  Icons.format_shapes,
                  color: Colors.yellowAccent,
                  size: 19,
                ),
              ),
              Text(
                icerik,
                style: const TextStyle(
                  color: Colors.yellowAccent,
                  fontSize: 16,
                ),
              )
            ] else if (icerik == "ETKİNLİK") ...[
              const Padding(
                padding: EdgeInsets.fromLTRB(5.0, 4, 2, 4),
                child: Icon(
                  Icons.extension_outlined,
                  color: Colors.yellowAccent,
                  size: 21,
                ),
              ),
              Text(
                icerik,
                style: const TextStyle(
                  color: Colors.yellowAccent,
                  fontSize: 16,
                ),
              )
            ] else if (icerik == "AÇIKLAMA") ...[
              const Padding(
                padding: EdgeInsets.fromLTRB(5.0, 4, 2, 4),
                child: Icon(
                  Icons.help_outline_outlined,
                  color: Colors.yellowAccent,
                  size: 21,
                ),
              ),
              Text(
                icerik,
                style: const TextStyle(
                  color: Colors.yellowAccent,
                  fontSize: 16,
                ),
              )
            ] else ...[
              const Padding(
                padding: EdgeInsets.fromLTRB(5.0, 4, 2, 4),
                child: Icon(
                  Icons.panorama_fish_eye,
                  color: Colors.yellowAccent,
                  size: 21,
                ),
              ),
              Text(
                icerik,
                style: const TextStyle(
                  color: Colors.yellowAccent,
                  fontSize: 16,
                ),
              )
            ]
          ],
        ),
      ),
    );
  }

  // Kazanım sayfasında Kazanımı yazdıran widget döngü üzerinde kullanılmakta
  Widget textWidget(BuildContext context, String icerik) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15.0, 0, 6.0, 0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          "● $icerik",
          textAlign: TextAlign.left,
          style: const TextStyle(color: Colors.white, fontSize: 17),
        ),
      ),
    );
  }
}
