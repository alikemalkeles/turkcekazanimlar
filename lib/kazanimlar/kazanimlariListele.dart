// ignore: file_names
import 'dart:convert';
import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:turkce_kazanimlar/hariciUygulamalar/harici_uygulama_kuyosis.dart';


import '../database/gitlab.dart';
import '../database/hiveDatabase.dart';
import '../database/uzaktanGuncelleme.dart';
import '../hakkinda/uygulamaHakkinda.dart';
import '../kazanimKayit/kazanimlariKaydet.dart';
import '../kelimeFilitreleme/kelimeFilitrele.dart';
import '../model/modelDers.dart';
import '../model/modelKurs.dart';
import '../takvim/belirliGunHaftalar.dart';
import '../yazimKurallari/yazimKurallari.dart';
import 'dersKazanimiGoruntule.dart';
import 'kursKazanimiGoruntule.dart';

class KazanimlariListele extends StatefulWidget {
  const KazanimlariListele({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _KazanimlariListe createState() => _KazanimlariListe();
}

class _KazanimlariListe extends State<KazanimlariListele> {
// Tüm ders ve kurs kazanimlarını tutan listeler
  List<List<ModelDers>> tumDersKazanimlari = [];
  List<List<ModelKurs>> tumKursKazanimlari = [];

  Color scaffoldColorDers = Colors.indigo[600]!;
  Color containerColorDers = Colors.indigo[300]!;
  Color scaffoldColorKurs = Colors.teal[600]!;
  Color containerColorKurs = Colors.teal[300]!;

  Color scaffoldAppBarColor = const Color(0xffDAE8FF);


  var hiveDb = HiveDatabase();
  var gitlabDb = GitlabData();

  

  final String dosyaYolu =
      "https://gitlab.com/alikemalkeles/turkcekazanimlar/-/raw/main/kaynaklar/planlar.json";

// Bu fonksiyon assets klasoru içinden ders ve kurs kazanımlarını listeleyip sonra model üzerinden
// gerekli değişken üzerine aktarılacak

  Future kazanimlariGetir() async {

    Map<dynamic, dynamic> jsonData = {};

    var resultHive = await hiveDb.getDataHive("planlar");
    // print("Hive den dönen sonuç: ${resultHive}");
    // EĞER Hive den null dönerse
    // 1. İnternete bağlan verileri al ve Hiveye yaz
    // 2. İnternet yok ise assets üzerinden al ve hiveye yaz
    if (resultHive == null) {
      var dataGitlab = await gitlabDb.getGitlabData(dosyaYolu);
      // İnternetten gelen data null ise hive ye ekle null değil ise assetsten datayı al
      if (dataGitlab == null) {
        print("KazanimlariListele Hive boş, internet bağlantısı kurulamadı, içerikler assetsden alınacak");
        // İnternetten de veri gelmediği için aseets den verileri al
        final assetsPlanlar = await rootBundle.loadString("assets/kaynaklar/planlar.json");
        jsonData = jsonDecode(assetsPlanlar) ?? {};
        await hiveDb.addDataHive("planlar", jsonData);
      } else {
        print("KazanimlariListele Hive boş, içerikler internetten alınacak ve hiveye yazılacak");
        // İntrenetten data başarılı bir şekilde geldği için gelen data Hive'ye yazılacak.
        jsonData = await gitlabDb.getGitlabData(dosyaYolu);
        await hiveDb.addDataHive("planlar", jsonData);
      }
    } else {
      // Hive de içerik bulunduğundan hive döndürelecek
      jsonData = resultHive;
      print("KazanimlariListele Hive de içerik bulunuyor jsonType ${jsonData.runtimeType}");
    }

    tumDersKazanimlari.clear();
    tumKursKazanimlari.clear();



    // Json dosyasındaki anahtar kelimeler üzerinden döngü oluşturup her bir plana ulaşma
    for (var plan in jsonData["planlar"].keys) {
      // ignore: unused_local_variable
      var dongudekiPlan = jsonData["planlar"][plan];

      if (plan.toString().contains("ders") == true) {
        // ders kazanimlari
        // Sıradaki plan dosyasını satır satır okuyup her haftasını Modele aktarıp bu modeli
        // tamponDers üzerine atıp döngü bitmesiyle de tampon üzerindeki ModelPlan türündeki listeyi
        // tumDersKazanımları listesine aktarılacak
        List<ModelDers> tamponDers = [];
        for (var oge in dongudekiPlan) {
          tamponDers.add(ModelDers(
              oge[0],
              oge[1],
              oge[2],
              oge[3],
              oge[4],
              oge[5],
              oge[6],
              oge[7],
              oge[8],
              oge[9],
              oge[10],
              oge[11],
              oge[12],
              oge[13]));
        }
        // Haftalık kazımları model listesine yazdıktan sonra model listesini ana kazanım listesine gönder
        tumDersKazanimlari.add(tamponDers);
      } else {
        List<ModelKurs> tamponKurs = [];
        for (var oge in dongudekiPlan) {
          tamponKurs.add(ModelKurs(oge[0], oge[1], oge[2], oge[3], oge[4],
              oge[5], oge[6], oge[7], oge[8]));
        }
        // Haftalık kazımları model listesine yazdıktan sonra model listesini ana kazanım listesine gönder
        tumKursKazanimlari.add(tamponKurs);
      }
    }
    return "1";
  }

  
  var kontrol = UzaktanGuncellemeKontrol();

  @override
  void initState() {
    
    super.initState();
    kazanimlariGetir().then((value) {
      setState(() {
       // print(value);
        tumDersKazanimlari;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor:  scaffoldAppBarColor,
        appBar: AppBar(
          title: Padding(
            padding: EdgeInsets.fromLTRB(
                0, AppBar().preferredSize.height / 2 - 19, 0, 0),
            child: const AutoSizeText(
              "TÜRKÇE KAZANIMLAR",
              maxFontSize: 19,
              style: TextStyle(color: Colors.black87, fontSize: 17, fontWeight: FontWeight.w400),
            ),
          ),

          centerTitle: true,
          iconTheme: const IconThemeData(color: Colors.black87, size: 30),
          toolbarHeight: 42,
          
          elevation: 0.0,
          backgroundColor: scaffoldAppBarColor, //Color.fromRGBO(241, 219, 191, 1), 
          leading: Builder(
            builder: (context) => IconButton(
              icon: const Icon(
                Icons.widgets,
                color: Colors.black38,
                size: 27,
              ),
              onPressed: () => Scaffold.of(context).openDrawer(),
            ),
          ),
            
          bottom: TabBar(
             
              labelColor: Colors.black,
              labelStyle: const TextStyle(fontWeight: FontWeight.w700, fontSize: 15),
              unselectedLabelStyle: const TextStyle(fontWeight: FontWeight.w500, fontSize: 15),
              unselectedLabelColor: Colors.black.withOpacity(0.5),
              padding: const EdgeInsets.fromLTRB(18, 0, 18, 5),
              indicatorColor: Colors.black,
              dividerColor: Colors.transparent,
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(35.0),
                border: Border.all(color: Colors.white30, width: 2),

                gradient: LinearGradient(colors: [Colors.white, Colors.white.withOpacity(0.8)]),
                      boxShadow:  const [
                        BoxShadow(
                          color: Colors.black12,
                          spreadRadius: 2, //color of shadow//spread radius
                          blurRadius: 4, // blur radius
                          offset: Offset(0, 2), // changes position of shadow
                        ),
                        //you can set more BoxShadow() here
                      ],

              ),
              tabs: const [
                Tab(

                  height: 30,
                  text: "    DERS    ",

                ),
                Tab(

                  height: 30,
                  text: "    KURS    ",

                ),
              ]
            ),
        ),
        body: Container(
           margin:const  EdgeInsets.fromLTRB(1.0, 2.0, 1.0, 0.0),
           padding: const  EdgeInsets.fromLTRB(1.0, 0.0, 1.0, 0.0),
          decoration: BoxDecoration(
          color: scaffoldAppBarColor,
          borderRadius: const BorderRadius.only(
              topRight: Radius.circular(11.0), topLeft: Radius.circular(11.0)),
        ),
          child: TabBarView(
            
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: tabListDers(context), // Color.fromARGB(211, 114, 159, 209)
              ),
              Padding(
                padding:const  EdgeInsets.fromLTRB(0, 4, 0, 0),
                child: tabListKurs(context),
              ),
            ],
          ),
        ),
        drawer: drawerMenu(context),
      ),
    );
  }

  // Ders kazanımlarını listeler
  Widget tabListDers(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(2.0, 5.0, 2.0, 0.0),
      child: GridView.count(
        shrinkWrap: true,
        crossAxisCount: 3,
        scrollDirection: Axis.vertical,
        children: List.generate(tumDersKazanimlari.length, (index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        DersKazanimGoruntule(tumDersKazanimlari[index]),
                  ));
            },
            child: Container(
                padding:const  EdgeInsets.all(4.0),
                margin: const EdgeInsets.all(4.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  color: containerColorDers,
                  gradient: LinearGradient(
                      colors: [containerColorDers, scaffoldColorDers]),
                  boxShadow: [
                    BoxShadow(
                      color: scaffoldColorDers, //color of shadow
                      spreadRadius: 1, //spread radius
                      blurRadius: 3, // blur radius
                      offset: const Offset(0, 2), // changes position of shadow
                    ),
                    //you can set more BoxShadow() here
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    // Ders okuma veya yazma becerisi ise içerği ona göre listele
                    if (tumDersKazanimlari[index][index]
                        .yayinEvi
                        .isNotEmpty) ...[
                      Chip(
                        backgroundColor: scaffoldColorDers,
                        elevation: 6,
                        side: BorderSide.none,
                        label: const AutoSizeText(
                          "Türkçe",
                          maxLines: 1,
                          maxFontSize: 17,
                          minFontSize: 14,
                          style:
                              TextStyle(color: Colors.white, fontSize: 16),
                        ),
                        avatar: CircleAvatar(
                          backgroundColor: containerColorDers,
                          child: AutoSizeText(
                            "${tumDersKazanimlari[index][0].ders.substring(0, 1)}",
                            maxFontSize: 18,
                            minFontSize: 10,
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 17),
                          ),
                        ),
                      ),
                      // Yayınevi
                      Center(
                        child: AutoSizeText(
                          "${tumDersKazanimlari[index][index].yayinEvi}",
                          maxFontSize: 14,
                          minFontSize: 12,
                          textAlign: TextAlign.center,
                          maxLines: 2,
                          style: const TextStyle(
                              color: Colors.white, fontSize: 13),
                        ),
                      ),
                    ] else ...[
                      Chip(
                        backgroundColor: scaffoldColorDers,
                        side: BorderSide.none,
                        elevation: 6,
                        label: const Text(
                          "Tüm Sınıflar",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      // Yayınevi
                      Center(
                        child: Text(
                          "${tumDersKazanimlari[index][index].ders}",
                          softWrap: true,
                          textAlign: TextAlign.center,
                          style:
                              TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      ),
                    ]
                  ],
                )),
          );
        }),
      ),
    );
  }

  // Kurs kazanımlarını listeler
  Widget tabListKurs(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(2.0, 5.0, 2.0, 0.0),
      child: GridView.count(
        shrinkWrap: true,
        crossAxisCount: 3,
        scrollDirection: Axis.vertical,
        children: List.generate(tumKursKazanimlari.length, (index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        KursKazanimGoruntule(tumKursKazanimlari[index]),
                  ));
            },
            child: Container(
                padding: const EdgeInsets.all(4.0),
                margin: const  EdgeInsets.all(4.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  color: containerColorKurs,
                  gradient: LinearGradient(
                      colors: [containerColorKurs, scaffoldColorKurs]),
                  boxShadow: [
                    BoxShadow(
                      color: scaffoldColorKurs, //color of shadow
                      spreadRadius: 1, //spread radius
                      blurRadius: 3, // blur radius
                      offset: const Offset(0, 2), // changes position of shadow
                    ),
                    //you can set more BoxShadow() here
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Chip(
                      backgroundColor: scaffoldColorKurs,
                      side: BorderSide.none,
                      label: const AutoSizeText(
                        "Türkçe",
                        minFontSize: 14,
                        maxFontSize: 17,
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                      avatar: CircleAvatar(
                        backgroundColor: containerColorKurs,
                        child: Text(
                          "${tumKursKazanimlari[index][0].sinif}",
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 18),
                        ),
                      ),
                    ),
                    // Yayınevi
                    const Center(
                      child: Text(
                        "DYK Planı",
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ],
                )),
          );
        }),
      ),
    );
  }

  // Yan menu sayfası
  Widget drawerMenu(BuildContext context) {
    return Drawer(
      elevation: 10,
      backgroundColor: Colors.transparent,
      child: BlurryContainer(
        borderRadius: BorderRadius.circular(0),
        padding: const EdgeInsets.all(0.0),
        blur: 12,
        color: Colors.blue[200]!.withOpacity(0.3),
        child: ListView(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
          // Menü Ögeleri
          children: [
            const SizedBox(height: 35),
            Align(
              alignment: Alignment.center,
              child: Container(
                padding: const EdgeInsets.fromLTRB(0.0, 7.0, 0.0, 7.0),
                height: 110,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(0.0),
                      topLeft: Radius.circular(0.0),
                      bottomRight: Radius.circular(40.0),
                      topRight: Radius.circular(40.0),
                    )),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(50, 0, 50, 4),
                  child: Image.asset("assets/resimler/appIcon.png"),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                padding: const EdgeInsets.fromLTRB(0.0, 7.0, 0.0, 7.0),
                height: 35,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(18.0),
                      topLeft: Radius.circular(18.0),
                      bottomRight: Radius.circular(0.0),
                      topRight: Radius.circular(0.0),
                    )),
                child: const AutoSizeText(
                  "     YARDIMCI UYGULAMALAR     ",
                  minFontSize: 15,
                  maxFontSize: 17,
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
            ),

            const SizedBox(height: 35),
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>  TakvimApp(),
                  ),
                );
              },
              dense: true,
              selectedColor: Colors.deepOrange,
              leading: const Icon(
                Icons.calendar_today_outlined,
                color: Colors.white,
              ),
              title: const Text(
                "Akademik Takvim",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
              subtitle: const Text(
                "Belirli gün ve haftalar, Akademik takvim",
                style: TextStyle(fontSize: 12, color: Colors.white70),
              ),
              trailing: const Icon(
                Icons.arrow_right_sharp,
                color: Colors.white,
                size: 26,
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => KazanimKaydet(),
                  ),
                );
              },
              leading: const Icon(
                Icons.library_books_outlined,
                color: Colors.white,
              ),
              title: const Text(
                "Planlar ve Diğerleri",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
              subtitle: const Text(
                "Yıllık planlar, Sesli metinler, Ders kitapları(pdf)",
                style: TextStyle(fontSize: 12, color: Colors.white70),
              ),
              trailing: const Icon(
                Icons.arrow_right_sharp,
                color: Colors.white,
                size: 26,
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => YazimKurallari(),
                  ),
                );
              },
              leading: const Icon(
                Icons.text_format_outlined,
                color: Colors.white,
              ),
              title: const Text(
                "Yanlış Yazılan Kelimeler",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
              subtitle: const Text(
                "Yazımı karıştırılan kelimeler listesi ve 'Hangisi Doğru?'  oyunu",
                style: TextStyle(fontSize: 12, color: Colors.white70),
              ),
              trailing: const Icon(
                Icons.arrow_right_sharp,
                color: Colors.white,
                size: 26,
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => KelimeArama(),
                  ),
                );
              },
              leading: const Icon(
                Icons.filter_list_alt,
                color: Colors.white,
              ),
              title: const Text(
                "Sözcük Türetme",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
              subtitle: const Text(
                "Rastgele harflerle sözcük türetme",
                style: TextStyle(fontSize: 12, color: Colors.white70),
              ),
              trailing: const Icon(
                Icons.arrow_right_sharp,
                color: Colors.white,
                size: 26,
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => UygulamaHakkinda(),
                  ),
                );
              },
              leading: const Icon(
                Icons.aspect_ratio_outlined,
                color: Colors.white,
              ),
              title: const Text(
                "Uygulama Hakkında",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
              subtitle: const Text(
                "Uygulama hakkında bilgi ve iletişim",
                style: TextStyle(fontSize: 12, color: Colors.white70),
              ),
              trailing: const Icon(
                Icons.arrow_right_sharp,
                color: Colors.white,
                size: 26,
              ),
            ),

            Align(
              alignment: Alignment.center,
              child: Container(
                margin: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 12.0),
                padding: const EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 4.0),
                height: 30,
                decoration: BoxDecoration(
                    color: Colors.blueGrey[500]!.withOpacity(0.9),
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(18.0),
                      topLeft: Radius.circular(18.0),
                      bottomRight: Radius.circular(18.0),
                      topRight: Radius.circular(18.0),
                    )),
                child: const AutoSizeText(
                  "     Harici Uygulamalar     ",
                  minFontSize: 15,
                  maxFontSize: 17,
                  style: TextStyle(color: Colors.white70, fontSize: 16),
                ),
              ),
            ),

            //Küyösis uygulaması tanıtımı
            ListTile(
              onTap: () {
               Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const HariciUygulamalarKuyosis(),
                  ),
                );
              },
              leading: Image.asset("assets/resimler/kuyosis.png"),
              title: const Text(
                "KÜYÖSİS",
                style: TextStyle(color: Colors.pink, fontSize: 15, fontWeight: FontWeight.w500),
              ),
              subtitle: const Text(
                "Kütüphane Yönetim Uygulaması",
                style: TextStyle(fontSize: 13, color: Colors.white),
              ),
              trailing: const Icon(
                Icons.arrow_right_sharp,
                color: Colors.pink,
                size: 26,
              ),
            )


            // ListTile(
            //   onTap: () {
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (context) => DestekteBulunun(),
            //       ),
            //     );
            //   },
            //   leading: const Icon(
            //     Icons.volunteer_activism,
            //     color: Colors.red,
            //   ),
            //   title: const Text(
            //     "Bağış",
            //     style: TextStyle(color: Colors.white, fontSize: 14),
            //   ),
            //   subtitle: const Text(
            //     "Uygulamanın sürekliliği için bağışta bulun",
            //     style: TextStyle(fontSize: 12, color: Colors.white70),
            //   ),
            //   trailing: const Icon(
            //     Icons.arrow_right_sharp,
            //     color: Colors.white,
            //     size: 26,
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
