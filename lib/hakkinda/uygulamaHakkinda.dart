// ignore: file_names
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:url_launcher/url_launcher.dart';

import '../database/uzaktanGuncelleme.dart';
// import 'package:url_launcher/url_launcher_string.dart';

// ignore: use_key_in_widget_constructors
class UygulamaHakkinda extends StatefulWidget {
  @override
  // ignore: library_private_types_in_public_api
  _UygulamaHakkindaState createState() => _UygulamaHakkindaState();
}

class _UygulamaHakkindaState extends State<UygulamaHakkinda> {
  // Db sürümünü kontrol etme
  var uzaktanGuncelle = UzaktanGuncellemeKontrol();
  Map<String, List<dynamic>> surumler = {"planlar": [0, null], "takvim": [0, null], "yollar": [0, null], "kelimeler": [0, null]};

  final String version = "1.0.13";

  Future surumKontrolPaneli() async {
    var surumData = await uzaktanGuncelle.uzaktanHepsiniGuncelle();
    return surumData;
    // print("adım1 ${surumData[0]}- ${surumData[1]}");
  }

  Future<void> _launchInBrowser(Uri url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Adres bulunamadı $url';
    }
  }

  Future<void> _sendMail() async {
    final url = Uri(
      scheme: 'mailto',
      path: 'alikemalkeles@gmail.com',
      query: 'subject=Dilek ve Şikayetler&body=Görüşlerinize ihtiyacımız var.',
    );
    if (await canLaunchUrl(url)) {
      launchUrl(url);
    } else {
      // ignore: avoid_print
      print("Can't launch $url");
    }
  }

  @override
  void initState() {
    super.initState();
    surumKontrolPaneli().then((value) {
      setState((){
        print("Uygulama Hakkında fonk. gelen: ${value}");
          surumler = value;
      });
      
    });
    // surumKontrol();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/resimler/arkplan1.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          toolbarHeight: 35,
          elevation: 0.0,
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(Icons.arrow_back_ios, color: Colors.white)),
        ),
        body: hakkinda(context),
      ),
    );
  }

  //
  Widget hakkinda(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // Icon(Icons.menu_book, color: Colors.pinkAccent, size: 100,),
          Padding(
            padding: const EdgeInsets.fromLTRB(90, 0, 90, 4),
            child: InkWell(
              onDoubleTap: (){
                _launchInBrowser(Uri.parse(
                                    "https://gitlab.com/alikemalkeles/turkcekazanimlar"));
              },
              child: Image.asset("assets/resimler/appIcon.png")
              ),
          ),

          // Uygulama Adı
          BlurryContainer(
            blur: 15,
            elevation: 0.0,
            color: Colors.white70.withOpacity(0.2),
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            borderRadius: const BorderRadius.all(Radius.circular(30)),
            child: const Text(
              "TÜRKÇE KAZANIMLAR",
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.w800),
            ),
          ),

          // Hakkında
          Padding(
            padding: const EdgeInsets.fromLTRB(14, 25, 14, 20),
            child: BlurryContainer(
              blur: 16,
              elevation: 0.0,
              color: Colors.transparent,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              padding: const EdgeInsets.all(0.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white70.withOpacity(0.3),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 15, 8, 15),
                  child: Column(
                    children: [
                      const ListTile(
                          dense: true,
                          leading: Icon(Icons.developer_board_outlined,
                              color: Colors.pinkAccent, size: 28),
                          title: Text("Uygulama Geliştiricisi",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black)),
                          subtitle: Text(
                            "Ali Kemal KELEŞ",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black),
                          )),
                       ListTile(
                          dense: true,
                          leading: const Icon(Icons.android,
                              color: Colors.pinkAccent, size: 28),
                          title: const Text("Sürüm",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black)),
                          subtitle: Text(
                            "${version}",
                            style: const TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black),
                          )),
                      ListTile(
                          dense: true,
                          leading: SizedBox(
                            width: 29,
                            child: IconButton(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                              icon: const Icon(
                                Icons.email_outlined,
                                size: 28,
                                color: Colors.pinkAccent,
                              ),
                              onPressed: () {
                                _sendMail();
                              },
                            ),
                          ),
                          title: const Text("İletişim",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black)),
                          subtitle: const Text(
                            "alikemalkeles@gmail.com",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black),
                          )),
                     /* ListTile(
                          dense: true,
                          leading: SizedBox(
                            width: 29,
                            child: IconButton(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                              icon: const Icon(
                                Icons.location_history_outlined,
                                size: 28,
                                color: Colors.pinkAccent,
                              ),
                              onPressed: () {
                                _launchInBrowser(Uri.parse(
                                    "https://m.facebook.com/alikemal.keles.9"));
                              },
                            ),
                          ),
                          title: const Text("Sosyal Medya",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black87)),
                          subtitle: const Text(
                            "İnstagram: @akemalkeles\nYaay: @A_Kemal",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black87),
                          )
                      ),*/
                      Theme(
                        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
                        child: ExpansionTile(
                          textColor: Colors.black45,
                          expandedAlignment: Alignment.center,
                          leading: const SizedBox(
                            width: 48,
                            child: IconButton(
                              padding: EdgeInsets.fromLTRB(10, 0, 15, 0),
                              icon: Icon(
                                Icons.cloud_done_outlined,
                                size: 28,
                                color: Colors.pinkAccent,
                              ),
                              onPressed: null,
                            ),
                          ),
                          iconColor: Colors.black,
                          collapsedIconColor: Colors.black38,
                          tilePadding: const EdgeInsets.all(7),
                          expandedCrossAxisAlignment: CrossAxisAlignment.center,
                          childrenPadding: const EdgeInsets.fromLTRB(20, 0, 15, 0),
                          title: const Text(
                            "Güncelleme Durumu",
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                                color: Colors.black),
                          ),
                          /* subtitle: const Text("",
                              style: TextStyle(color: Colors.black54,  fontWeight: FontWeight.w300, fontSize: 14),
                            ), */
                          children: [
                            ListTile(
                              dense: true,
                              leading: Icon(Icons.check_circle, color: surumler["planlar"]![0] == 1 ?
                               Colors.greenAccent : Colors.redAccent,
                              size: 25,
                              ),
                              title: const Text(
                                "Yıllık Planlar",
                                style:
                                    TextStyle(color: Colors.black, fontSize: 14),
                              ),
                              subtitle: Text(
                                        "${surumler["planlar"]![1]}",
                                        style: const TextStyle(color: Colors.black, fontSize: 12),
                              )
                            ),
                            ListTile(
                              dense: true,
                              leading: Icon(Icons.check_circle, color: surumler["takvim"]![0] == 1 ?
                              Colors.greenAccent : Colors.redAccent,
                              size: 25,
                              ),
                              title: const Text(
                                "Akademik Takvim",
                                style:
                                    TextStyle(color: Colors.black, fontSize: 14),
                              ),
                              subtitle: Text(
                                        "${surumler["takvim"]![1]}",
                                        style: const TextStyle(color: Colors.black54, fontSize: 12),
                              )
                            ),
                            ListTile(
                              dense: true,
                              leading: Icon(Icons.check_circle, color: surumler["yollar"]![0] == 1 ?
                              Colors.greenAccent : Colors.redAccent,
                              size: 25,
                              ),
                              title: const Text(
                                "Planlar ve Diğerleri",
                                style:
                                    TextStyle(color: Colors.black, fontSize: 14),
                              ),
                              subtitle: Text(
                                        "${surumler["yollar"]![1]}",
                                        style: const TextStyle(color: Colors.black54, fontSize: 12),
                              )
                            ),
                            ListTile(
                              dense: true,
                              leading: Icon(Icons.check_circle, color: surumler["kelimeler"]![0] == 1 ?
                               Colors.greenAccent : Colors.redAccent,
                              size: 25,
                              ),
                              title: const Text(
                                "Yanlış Yazılan Kelimeler",
                                style:
                                    TextStyle(color: Colors.black, fontSize: 14),
                              ),
                              subtitle: Text(
                                        "${surumler["kelimeler"]![1]}",
                                        style: const TextStyle(color: Colors.black54, fontSize: 12),
                              )
                            ),
                      
                      
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(14, 25, 14, 20),
            child: BlurryContainer(
              blur: 10,
              elevation: 0,
              color: Colors.transparent,
              borderRadius: const BorderRadius.all(Radius.circular(12)),
              padding: const EdgeInsets.all(0.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white60.withOpacity(0.3),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 15, 8, 15),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      BlurryContainer(
                        //width: MediaQuery.of(context).size.width -,
                        blur: 17,
                        elevation: 0.0,
                        color: Colors.blueAccent.withOpacity(0.3),

                        padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(30)),
                        child: const AutoSizeText(
                          "YARDIMCI UYGULAMALAR HAKKINDA",
                          minFontSize: 12,
                          maxFontSize: 18,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.black,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                     const  Align(
                        alignment: Alignment.centerLeft,
                        child: Wrap(
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            Icon(
                              Icons.arrow_right_outlined,
                              size: 40,
                              color: Colors.pink,
                            ),
                            Text(
                              "Akademik Takvim",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.fromLTRB(20, 4, 10, 10),
                        child: Text(
                            " Eğitim Öğretim Takvimini ile Belirli Gün ve Haftaları kolayca takip edebilirsiniz.",
                            style: TextStyle(color: Colors.black87)),
                      ),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Wrap(
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            Icon(
                              Icons.arrow_right_outlined,
                              size: 40,
                              color: Colors.pink,
                            ),
                            Text(
                              "Planlar ve Diğerleri",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.fromLTRB(20, 4, 10, 10),
                        child: Text(
                            " Bu uygulamayla ders planları, kurs planları, sesli metinler ve derslerin pdf'lerini kolayca telefonunuza indirebilir ya da indirme bağlantısını paylaşabilirsiniz.",
                            style: TextStyle(color: Colors.black87)),
                      ),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Wrap(
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children:  [
                            Icon(
                              Icons.arrow_right_outlined,
                              size: 40,
                              color: Colors.pink,
                            ),
                            Text(
                              "Yanlış Yazılan Kelimeler",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.fromLTRB(20, 4, 10, 10),
                        child: Text(
                            " Dilimizde sıkça yapılan yazım yanlışlarının listesi uygulama üzerinde liste halinde sunulmuştur. Ayrıca 'Kendini Sına' yardımcı uygulamasıyla oyun tadında kendimizi sınayabiliriz.",
                            style: TextStyle(color: Colors.black87)),
                      ),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Wrap(
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children:  [
                            Icon(
                              Icons.arrow_right_outlined,
                              size: 40,
                              color: Colors.pink,
                            ),
                            Text(
                              "Kelime Filitreleme",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.fromLTRB(20, 4, 10, 10),
                        child: Text(
                            " Kelime Filitreleme uygulaması, rastgele girilen karekterlerden veya sırası değiştirilmiş karekterlerden TDK Sözlük üzerinde yer alan yaklaşık 92.000 kelimeyi kullanarak mantıklı sözcükler oluşturmaktadır.",
                            style: TextStyle(color: Colors.black87)),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
