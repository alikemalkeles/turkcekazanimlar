
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'database/uzaktanGuncelleme.dart';
import 'kazanimlar/kazanimlariListele.dart';



void main() async {

  const boxName = "verikutusu";
  await Hive.initFlutter();
  await Hive.openBox(boxName);
 // Ders ve Kurs planlarını tutar

  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent, // transparent status bar
    systemNavigationBarColor:  Color(0xffDAE8FF),
    
    systemNavigationBarIconBrightness: Brightness.dark,
    
  ));

  runApp(const DersKazanimlari(key: null,));
}

class DersKazanimlari extends StatefulWidget {
  const DersKazanimlari({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _DersKazanimlari createState() => _DersKazanimlari();
}

class _DersKazanimlari extends State<DersKazanimlari> {

  var uzaktanGuncelle = UzaktanGuncellemeKontrol();

  @override
  void initState(){
    super.initState();
    Future.delayed(const Duration(seconds: 5),(){
       uzaktanGuncelle.uzaktanHepsiniGuncelle();
    });
  }

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
           dividerColor: Colors.transparent
        ),
      home: const KazanimlariListele(),
    );
  }
}
