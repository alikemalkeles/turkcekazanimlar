// ignore: file_names
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';


class HiveDatabase {
  
  static const boxName = "verikutusu";
  final  boxPlan = Hive.box(boxName);

  /// Add new Task
  Future<void> addDataHive(String dbAnahtar, var islem) async {
    await boxPlan.put(dbAnahtar, islem);
  }

  /// Show task
  // ignore: missing_return
  Future getDataHive(String dbAnahtar) async {
    var data = await boxPlan.get(dbAnahtar);
    return data;
  }


  /// Delete task
  Future<void> deleteDataHive(String dbAnahtar) async {
    await boxPlan.delete(dbAnahtar);
  }

  Future<void> hiveadbClose() async {
    await boxPlan.close();
  }

//
}

