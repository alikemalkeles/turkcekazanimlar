
import 'dart:convert';

import 'gitlab.dart';
import 'hiveDatabase.dart';

class UzaktanGuncellemeKontrol{


  /*
    Not: Gitlab üzerinden gelen veri Map formatında olduğu için json işlemine tabi değil
         eğer assets üzerinden gelecekse sitring olduğu için json olarak kodla
  */

  var hiveDb = HiveDatabase();
  var gitlabDb = GitlabData();

  
  Map<String, String> tumYollar = {
    "planlar": "https://gitlab.com/alikemalkeles/turkcekazanimlar/-/raw/main/kaynaklar/planlar.json",
    "takvim": "https://gitlab.com/alikemalkeles/turkcekazanimlar/-/raw/main/kaynaklar/akademikTakvim.json",
    "yollar": "https://gitlab.com/alikemalkeles/turkcekazanimlar/-/raw/main/kaynaklar/bulutDosyaYollari.json",
    "kelimeler": "https://gitlab.com/alikemalkeles/turkcekazanimlar/-/raw/main/kaynaklar/yazimiYanlislar.json",
    "version": "https://gitlab.com/alikemalkeles/turkcekazanimlar/-/raw/main/kaynaklar/version.json"
  };

  

  final planAnahtar = "planlar";
  final takvimAnahtar = "takvim";
  final yollarAnahtar = "yollar";
  final kelimelerAnahtar = "kelimeler";

  

  Future uzaktanHepsiniGuncelle() async {

    var versionlar = await gitlabDb.getGitlabData(tumYollar["version"]!);


    Map<String, List>  guncellemeSonucu = {};
    // İlk Once Gtlabtan version dosyasını oku ve sürüm numaralarını al
    // Ve null kontrolü yap
    
    print("Uzaktan Güncelleme fonk. gitlabData ${versionlar}");
    if (versionlar != null){

      // Uzak sunucudan alınan dosyaları json olarak kodla ve döngü üzerinde kontrol et
      for (var v in versionlar.keys){

    
        // Sırayla hiveden içerikleri al ve sürümleri karşılaştır. ( Anahtar aynı)
        var getData = await hiveDb.getDataHive(v);
      //  print("Uzaktan güncelleme fonk. GetDataFonk ${getData.runtimeType}");
      //  print("${v}  sürüm Hive:- ${getData["sürüm"]}  - sürüm Gitlab: ${versionlar[v]}");
        
        // Hiveden gelen dataya null kontrolü
        if (getData == null){
          print("${v} - Güncelleme sırasında hiveden veri alınmadı");
          guncellemeSonucu[v] = [0, null];

        } else{
            // 
           if (getData["sürüm"] == versionlar[v]){
            print("${v} - Sürümler eşit");

            guncellemeSonucu[v] = [1, getData["sürüm"]];

        } else{
            print("${v} - Sürümler eşit değil o yuzden güncellenecek");
            // Sürümler eşit olmadığı için güncelleme işlemi yapılcak
            var islem = await gitlabDb.getGitlabData(tumYollar[v]!);

          //  print("Gitlabtan gelen veritini türü: ${islem.runtimeType}");
            
            if (islem != null ){
              
              await hiveDb.addDataHive(v, islem);
              guncellemeSonucu[v] = [1, getData["sürüm"]];
            } else{
              guncellemeSonucu[v] = [0, getData["sürüm"]];
            }

            print("${v} - Sürümler eşit değil o yuzden güncellenecek");

        }

        }


      } 

    } else{
      // Eğer null dönerse gitlabtan veri alınmadı mesajı döndür
      print("Gittalbtan sürüm dosyası alınamadı");
      guncellemeSonucu = {"planlar": [0, null], "takvim": [0, null], "yollar": [0, null], "kelimeler": [0, null]};

    }

    print("Güncelleme sonucu: ${guncellemeSonucu}");

    return guncellemeSonucu;


  }
//

}