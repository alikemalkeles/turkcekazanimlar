import 'dart:convert';

import 'package:http/http.dart' as http;

class GitlabData {


  Future getGitlabData(String dosyaYolu) async {
    

    String gitlabFilePath = dosyaYolu;

    http.Response response;
    // Link üzerinden dosyayı indir
    try {
      var url = Uri.parse(gitlabFilePath);
      response = await http.get(url);
      if (response.statusCode == 200 && response != null){
        Map<dynamic, dynamic> jsonData = jsonDecode(response.body);
        return jsonData;
        
      } else{
        print("Bağlantı başarısız oldu ${response.statusCode}");
        return null;
      }
      // print(response.body);
    } on Exception catch(hata){
      print("bilinmeyen bir bağlantı hatası ${hata}");
      return null;
    }


  }
}
