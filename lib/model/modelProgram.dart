//
class ModelProgram {
  late String gun;
  late String ders1;
  late String ders2;
  late String ders3;
  late String ders4;
  late String ders5;
  late String ders6;
  late String ders7;


  ModelProgram(this.gun, this.ders1, this.ders2, this.ders3, this.ders4,
      this.ders5, this.ders6, this.ders7);

  // Modelden harita türünde (sözlük) fonksiyon döndürme
  Map<String, String> toMap_() {
    var map = Map<String, String>();
    map["gun"] = gun;
    map["ders1"] = ders1;
    map["ders2"] = ders2;
    map["ders3"] = ders3;
    map["ders4"] = ders4;
    map["ders5"] = ders5;
    map["ders6"] = ders6;
    map["ders7"] = ders7;
    return map;
  }



  // Db den gelen map türündeki ogeyi modele aktarma
  ModelProgram.toFromModel(dynamic data){
    gun = data["gun"];
    ders1 = data["ders1"];
    ders2 = data["ders2"];
    ders3 = data["ders3"];
    ders4 = data["ders4"];
    ders5 = data["ders5"];
    ders6 = data["ders6"];
    ders7 = data["ders7"];

  }

}

class ModelDers{
  String ders;
  String sinif;
  String baslamaSaati;
  String bitisSaati;
  String cins;
  String saat;
  ModelDers(this.ders, this.sinif, this.baslamaSaati, this.bitisSaati, this.cins, this.saat);
}
